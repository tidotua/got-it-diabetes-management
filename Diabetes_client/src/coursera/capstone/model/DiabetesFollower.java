package coursera.capstone.model;

public class DiabetesFollower {
	private long id;
	private long uid;
	private long followerUid;
	private boolean approved;
	private boolean showMood;
	private boolean showMeal;
	private boolean showAdministerInsuline;
	private String fullName;
	
	public DiabetesFollower() {
		super();
	}
	
	public DiabetesFollower(long uid, long fuid) {
		super();
		this.uid = uid;
		this.followerUid = fuid;
	}
	
	public void setFields(DiabetesFollower source) {
		this.approved = source.approved;
		this.showMood = source.showMood;
		this.showMeal = source.showMeal;
		this.showAdministerInsuline = source.showAdministerInsuline;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public long getFollowerUid() {
		return followerUid;
	}

	public void setFollowerUid(long followerUid) {
		this.followerUid = followerUid;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean isApproved) {
		this.approved = isApproved;
	}

	public boolean isShowMood() {
		return showMood;
	}

	public void setShowMood(boolean showMood) {
		this.showMood = showMood;
	}

	public boolean isShowMeal() {
		return showMeal;
	}

	public void setShowMeal(boolean showMeal) {
		this.showMeal = showMeal;
	}

	public boolean isShowAdministerInsuline() {
		return showAdministerInsuline;
	}

	public void setShowAdministerInsuline(boolean showAdministerInsuline) {
		this.showAdministerInsuline = showAdministerInsuline;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
}
