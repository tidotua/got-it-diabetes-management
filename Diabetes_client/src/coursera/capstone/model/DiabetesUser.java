package coursera.capstone.model;

import coursera.capstone.utils.DiabetesConstants;

public class DiabetesUser {
	public enum UserType {
		USER_TYPE_TEEN,
		USER_TYPE_FOLLOWER,
	};
	
	private long id = -1;
	private String username = "";
	private String password = "";
	
	private String firstName;
	private String lastName;
	private long birthDay;
	private String medicalRecord;
	private long type;
	private long notificationPeriodMs = DiabetesConstants.MAX_CHECKIN_TIMEOUT;
	private boolean notificationIgnoreNight = true;
	
	public DiabetesUser() {
		
	}
	
	public DiabetesUser(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public long getId() {
		return id;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(long birthDay) {
		this.birthDay = birthDay;
	}

	public String getMedicalRecord() {
		return medicalRecord;
	}

	public void setMedicalRecord(String medicalRecord) {
		this.medicalRecord = medicalRecord;
	}

	public UserType getType() {
		return UserType.values()[(int)type];
	}

	public void setType(UserType type) {
		this.type = type.ordinal();
	}
	
	public void setType(long type) {
		this.type = type;
	}
	
	public long getNotificationPeriodMs() {
		return notificationPeriodMs;
	}

	public void setNotificationPeriodMs(long notificationPeriodMs) {
		this.notificationPeriodMs = notificationPeriodMs;
	}

	public boolean isNotificationIgnoreNight() {
		return notificationIgnoreNight;
	}

	public void setNotificationIgnoreNight(boolean notificationIgnoreNight) {
		this.notificationIgnoreNight = notificationIgnoreNight;
	}

	@Override
	public String toString() {
		return "DiabetesUser [id=" + id + ", username=" + username
				+ ", firstName=" + firstName + ", lastName=" + lastName
				+ ", birthDay=" + birthDay + ", medicalRecord=" + medicalRecord
				+ ", type=" + type + "]";
	}	
	
}
