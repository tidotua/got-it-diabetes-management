package coursera.capstone.view.control;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TreeMap;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Scroller;

public class DailyLinearPlotView extends View {
	
	private static final String TAG = DailyLinearPlotView.class.getSimpleName();
	
	private final long DAY_MS = 24 * 60 * 60 * 1000;
	private final float DAY_X_MARGIN = 0.1f;
	private final long DAY_MS_MARGIN = (long)(DAY_MS * DAY_X_MARGIN);
	private final long VISIBLE_X_MS = (long)(DAY_MS * (1 + 2 * DAY_X_MARGIN));
	private final float MARK_SIZE = 8.f;
	private final float LINE_WIDTH = 1.5f;
	private final float TEXT_HEIGHT = 20.f;
	private final SimpleDateFormat dateFormater = new SimpleDateFormat("MM/dd/yy", Locale.US);
	
	public interface LinearPlotListner {
		void onScroll(DailyLinearPlotView plotView, long newestVisibleItemTime);
		void onScrollStateChanged(DailyLinearPlotView plotView, int newState);
	};
	
	private class DailyValue {
		
		private long timePart;
		private long datePart;
		private float value;

		public DailyValue(long time, float value) {
			super();
			this.value = value;
			splitTime(time);
		}
		
		void splitTime(long time) {
	        datePart = getDayTime(time);
	        timePart = time - datePart; 
		}

		public long getTotalTime() {
			return datePart + timePart;
		}
		
		public long getDatePart() {
			return datePart;
		}
		
		public long getTimePart() {
			return timePart;
		}

		public float getValue() {
			return value;
		}
	};
	
	private class PlotPoint {
		
		private PointF point;
		private int color;
		
		public PlotPoint(PointF point, int color) {
			super();
			this.point = point;
			this.color = color;
		}
		
		public PointF getPoint() {
			return point;
		}
		
		public int getColor() {
			return color;
		}		
	};
	
	private class DateMark {
		
		private long dateTime;
		private float x;
		private String dateText;
		
		public DateMark(long dateTime, float x) {
			super();
			this.dateTime = dateTime;
			this.x = x;
			this.dateText = dateFormater.format(new Date(dateTime));
		}
		
		public long getDateTime() {
			return dateTime;
		}
		
		public float getX() {
			return x;
		}
		
		public String getDateText() {
			return dateText;
		}
	};
	
	public static long getDayTime(long time) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(time));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime().getTime();
	}
	
	private TreeMap<Long, DailyValue> values = new TreeMap<Long, DailyValue>();
	private float visibleYBoundMin = 0;
	private float visibleYBoundMax = 100;
	private float criticalYBoundMin = 20;
	private float criticalYBoundMax = 80;
	private PointF toPixelScale = new PointF(1, 1);
	private ArrayList<PlotPoint> visiblePoints = new ArrayList<PlotPoint>();
	private ArrayList<DateMark> visibleDates = new ArrayList<DateMark>();
	private long visibleTimeStart = 0;
	private long visibleTimeEnd = 0;
	private long visibleTimeStartMin = 0;
	private long visibleTimeStartMax = 0;
	private Paint fillPaint;
	private Paint linePaint;
	private Paint strokePaint;
	private Paint textPaint;
	private long maxVisiblePointTime = 0;
	private LinearPlotListner linearPlotListner;
	private int scrollState = OnScrollListener.SCROLL_STATE_IDLE;
	private Scroller scroller;
	private ValueAnimator scrollAnimator;

	private int backColor = 0xffffffff;
    private int criticalBackColor = 0x11ff0000;
    private int markColor = 0xff444444;
    private int markCriticalColor = 0xffff7777;
    private int lineColor = 0xff999999;
    private int dateLineColor = 0xffaaaaaa;
    private int dateTextColor = 0xff444444;
    
    
	public DailyLinearPlotView(Context context) {
		super(context);
		init();
	}
	
	public DailyLinearPlotView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	public DailyLinearPlotView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}
	
	@Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		updateScale();
		updatePlot();
		super.onSizeChanged(w, h, oldw, oldh);
    }
	
	@Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawPlot(canvas);
    }
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP)
		{
			changeScrollState(OnScrollListener.SCROLL_STATE_IDLE);
		}
		if (null != gestureDetector) {
			return gestureDetector.onTouchEvent(event);
		}
		return super.onTouchEvent(event);
	}
	
	private void changeScrollState(int state) {
		if (scrollState != state) {
			scrollState = state;
			updateScrollState();
		}
	}
	
	private void updateScrollState() {
		if (null != linearPlotListner) {
			linearPlotListner.onScrollStateChanged(DailyLinearPlotView.this, scrollState);
		}
	}
	
	public void setLinearPlotListner(LinearPlotListner linearPlotListner) {
		this.linearPlotListner = linearPlotListner;
	}
	
	private void init() {
		fillPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		fillPaint.setStyle(Paint.Style.FILL);
		fillPaint.setColor(backColor);
		
		linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		linePaint.setStyle(Paint.Style.FILL);
		linePaint.setColor(lineColor);
		linePaint.setStrokeWidth(LINE_WIDTH);
		
		strokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		strokePaint.setStyle(Paint.Style.STROKE);
		strokePaint.setColor(dateLineColor);
		strokePaint.setPathEffect(new DashPathEffect(new float[] {20, 10}, 0));
		
		textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		textPaint.setStyle(Paint.Style.FILL);
		textPaint.setColor(dateTextColor);
		textPaint.setTextAlign(Paint.Align.CENTER);
		textPaint.setTextSize(TEXT_HEIGHT);
		
		scroller = new Scroller(getContext(), null, true);
		scrollAnimator = ValueAnimator.ofFloat(0,1);
		scrollAnimator.addUpdateListener(animatorUpdateListener);		
		
        reset();
	}
	
	public void reset() {
		values.clear();
		visiblePoints.clear();
		visibleDates.clear();
	}
	
	public void addValue(long time, float value) {
		values.put(time, new DailyValue(time, value));
	}
	
	public void setVisibleYBounds(float minY, float maxY) {
		visibleYBoundMin = minY;
		visibleYBoundMax = maxY;
		rebuildPlot();
	}
	
	public void setCriticalYBounds(float minY, float maxY) {
		criticalYBoundMin = minY;
		criticalYBoundMax = maxY;
		rebuildPlot();
	}
	
	public void rebuildPlot() {
		if (!values.isEmpty()) {
			visibleTimeStart = values.lastEntry().getValue().getDatePart() - DAY_MS_MARGIN;
			visibleTimeEnd = visibleTimeStart + VISIBLE_X_MS;
			visibleTimeStartMin = values.firstEntry().getValue().getDatePart() - DAY_MS_MARGIN;
			visibleTimeStartMax = visibleTimeStart;
		}
		updateScale();
		updatePlot();
	}
	
	private void updatePlot() {
		rebuildDateMarks();
		rebuildPoints();
		invalidate();
	}
	
	private void updateScale() {
		toPixelScale.set(
				1.f * getWidth() / VISIBLE_X_MS,
				1.f * getHeight() / (visibleYBoundMax - visibleYBoundMin));
	}
	
	private void rebuildDateMarks() {
		visibleDates.clear();
		long dayTime = getDayTime(visibleTimeStart);
		for(int i = 0; i < 2; ++i) {
			dayTime += DAY_MS;
			if (dayTime > visibleTimeStart && dayTime < visibleTimeEnd) {
				float x = (dayTime - visibleTimeStart) * toPixelScale.x;
				visibleDates.add(new DateMark(dayTime, x));
			}
		}
	}
	
	private void rebuildPoints() {
		visiblePoints.clear();
		Long priorKey = null;
		long priorMaxVisiblePointTime = maxVisiblePointTime;
		for(Long key: values.keySet()) {
			if (key > visibleTimeStart) {
				if (visiblePoints.isEmpty() && null != priorKey) {
					addPoint(priorKey);
				}				
				addPoint(key);
				if (key >= visibleTimeEnd) {
					break;
				}
				maxVisiblePointTime = key;
			}
			priorKey = key;
		}
		
		if (null != linearPlotListner && priorMaxVisiblePointTime != maxVisiblePointTime) {
			linearPlotListner.onScroll(this, maxVisiblePointTime);
		}
	}
	
	private void addPoint(long key) {
		float value = values.get(key).getValue();
		PointF point = new PointF(
				(key - visibleTimeStart) * toPixelScale.x, 
				(visibleYBoundMax - visibleYBoundMin - value) * toPixelScale.y);
		int color = (value < criticalYBoundMin || value > criticalYBoundMax) ? markCriticalColor : markColor;
		visiblePoints.add(new PlotPoint(point, color));
	}
	
	protected void drawPlot(Canvas canvas) {
		float width = getWidth();
        float height = getHeight();
        RectF totalRect = new RectF(0, 0, width, height);
        
        fillPaint.setColor(backColor);
        fillPaint.setStyle(Paint.Style.FILL);
        canvas.drawRect(totalRect, fillPaint);
        fillPaint.setColor(criticalBackColor);
        
        RectF bottomCriticalRect = new RectF(
        		0, height - criticalYBoundMin * toPixelScale.y,
        		width, height);
        canvas.drawRect(bottomCriticalRect, fillPaint);
        
        RectF topCriticalRect = new RectF(
        		0, 0,
        		width, height - criticalYBoundMax * toPixelScale.y);
        canvas.drawRect(topCriticalRect, fillPaint);
        
        for(DateMark dateMark: visibleDates) {
        	Path path = new Path();
            path.moveTo(dateMark.getX(), TEXT_HEIGHT + 5);
        	path.lineTo(dateMark.getX(), height);
        	canvas.drawPath(path, strokePaint);
        }
        
        for(DateMark dateMark: visibleDates) {
        	canvas.drawText(dateMark.getDateText(), dateMark.getX(), TEXT_HEIGHT, textPaint);
        }
        
        PlotPoint priorPoint = null;
        for(PlotPoint point: visiblePoints) {
        	if (null != priorPoint) {
        		canvas.drawLine(priorPoint.getPoint().x, priorPoint.getPoint().y, point.getPoint().x, point.getPoint().y, linePaint);
        	}
        	priorPoint = point;
        }
        
        for(PlotPoint plotPoint: visiblePoints) {
        	fillPaint.setColor(plotPoint.getColor());
        	canvas.drawCircle(plotPoint.getPoint().x, plotPoint.getPoint().y, MARK_SIZE, fillPaint);
        }
	}
	
	private void scrollPlot(float distanceX) {
		long scrollTime = (long)(distanceX / toPixelScale.x);
		scrollPlotToTime(visibleTimeStart + scrollTime);
	}
	
	public long getVisibleTimeStart() {
		return visibleTimeStart;
	}
	
	public void scrollPlotToTime(long newTime) {
		long oldTimeValue = visibleTimeStart;
		visibleTimeStart = newTime;
		if (visibleTimeStart < visibleTimeStartMin) {
			visibleTimeStart = visibleTimeStartMin;
		} else if (visibleTimeStart > visibleTimeStartMax) {
			visibleTimeStart = visibleTimeStartMax;
		}
		visibleTimeEnd = visibleTimeStart + VISIBLE_X_MS;
		if (visibleTimeStart != oldTimeValue) {
			updatePlot();
		}
	}
	
	private void flingPlot(float velocityX) {
		if (!scroller.isFinished()) {
			scrollAnimator.cancel();
			scroller.forceFinished(true);
		}
		
		int startX = (int) ((visibleTimeStart - visibleTimeStartMin) * toPixelScale.x);
		int maxX = (int) ((visibleTimeStartMax - visibleTimeStartMin) * toPixelScale.x);
		scroller.fling(startX, 0, -(int)velocityX, 0, 0, maxX, 0, 0);
		scrollAnimator.setDuration(scroller.getDuration());
		scrollAnimator.start();
	}
	
	public void smoothScrollToTime(long timeMs) {
		if (!scroller.isFinished()) {
			scrollAnimator.cancel();
			scroller.forceFinished(true);
		}
		
		int startX = (int) ((visibleTimeStart - visibleTimeStartMin) * toPixelScale.x);
		int dx = (int) ((timeMs - visibleTimeStart) * toPixelScale.x);
		scroller.startScroll(startX, 0, dx, 0);
		scrollAnimator.setDuration(scroller.getDuration());
		scrollAnimator.start();
	}
	
	ValueAnimator.AnimatorUpdateListener animatorUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
		@Override
		public void onAnimationUpdate(ValueAnimator valueAnimator) {
			if (!scroller.isFinished()) {
				scroller.computeScrollOffset();
				scrollPlotToTime((long)(scroller.getCurrX() / toPixelScale.x + visibleTimeStartMin));
			} else {
				scrollAnimator.cancel();
				changeScrollState(OnScrollListener.SCROLL_STATE_IDLE);
			}
		}
	};
	
	public void smoothScrollToLastVisibleTimePoint(long lastVisiblePointTimeMs) {
		long nextPointTime = lastVisiblePointTimeMs;
		for(Long key: values.keySet()) {
			if (key > lastVisiblePointTimeMs) {
				nextPointTime = key;
				break;
			}
		}
		
		if (nextPointTime != lastVisiblePointTimeMs) {
			DailyValue lastValue = values.get(lastVisiblePointTimeMs);
			DailyValue nextValue = values.get(nextPointTime);
			if (null != lastValue && null != nextValue && lastValue.getDatePart() != nextValue.getDatePart()) {
				smoothScrollToDay(lastValue.getDatePart());
				return;
			}
		} else {
			nextPointTime += DAY_MS_MARGIN;
		}
		
		smoothScrollToTime(nextPointTime / 2 + lastVisiblePointTimeMs / 2 - VISIBLE_X_MS);
	}
	
	public void smoothScrollToDay(long dayTimeMs) {
		smoothScrollToTime(dayTimeMs - DAY_MS_MARGIN);
	}
	
	GestureDetector gestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			changeScrollState(OnScrollListener.SCROLL_STATE_FLING);
			flingPlot(velocityX);
			return super.onFling(e1, e2, velocityX, velocityY);
		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			changeScrollState(OnScrollListener.SCROLL_STATE_TOUCH_SCROLL);
			scrollPlot(distanceX);
			return super.onScroll(e1, e2, distanceX, distanceY);
		}

		@Override
		public boolean onDown(MotionEvent e) {
			return true;	// http://developer.android.com/training/custom-views/making-interactive.html
		}
	});

}
