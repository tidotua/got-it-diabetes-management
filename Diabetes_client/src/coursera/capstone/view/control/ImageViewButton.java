package coursera.capstone.view.control;

import coursera.capstone.R;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ImageViewButton extends ImageView {

    public ImageViewButton(Context context) {
        super(context);
    }

    public ImageViewButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageViewButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void drawableStateChanged() {
        setupColors();
        super.drawableStateChanged();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setupColors(){
        if (isEnabled() && !isSelected() && !isPressed()) {
            clearColorFilter();
            if (null != getBackground()) {
                getBackground().clearColorFilter();
            }
            return;
        }
        int color = getResources().getColor(!isEnabled() ? R.color.button_disabled : R.color.button_pressed);
        PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN);
        setColorFilter(colorFilter);
        if (null != getBackground()) {
            getBackground().setColorFilter(colorFilter);
        }
    }
}
