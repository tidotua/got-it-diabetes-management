package coursera.capstone.view.fragment;

import java.util.HashMap;

import android.app.Fragment;
import android.os.Bundle;

public class RetainedDataFragment extends Fragment {
	private HashMap<String, Object> dataMap = new HashMap<String, Object>();
	
	public void addData(String key, Object data) {
		dataMap.put(key, data);
	}
	
	public Object getData(String key) {
		return dataMap.get(key);
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}

