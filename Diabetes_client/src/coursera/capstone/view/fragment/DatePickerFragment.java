package coursera.capstone.view.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    public final static String DATE_FORMAT = "MM/dd/yy";
    private EditText editTextView;

    public void setEditTextView(EditText editTextView) {
        this.editTextView = editTextView;
    }
    
    public static Date parseDate(String dateText) {
    	SimpleDateFormat dateFormater = new SimpleDateFormat(DATE_FORMAT, Locale.US);
        Date date = new Date();
        try {
        	date = dateFormater.parse(dateText);
        } catch (ParseException e) {

        }
        return date;
    }
    
    public static String getDateText(Date date) {
    	SimpleDateFormat dateFormater = new SimpleDateFormat(DATE_FORMAT, Locale.US);
        return dateFormater.format(date);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();

        Date startDate = parseDate(editTextView.getText().toString());
        c.setTime(startDate);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (null != editTextView) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            editTextView.setText(getDateText(calendar.getTime()));
        }
    }
}
