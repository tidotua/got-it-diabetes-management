package coursera.capstone.view.fragment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import coursera.capstone.R;
import coursera.capstone.controller.DiabetesDataSource;
import coursera.capstone.model.DiabetesFollower;
import coursera.capstone.utils.DiabetesConstants;
import coursera.capstone.view.activity.FeedbackActivity;
import coursera.capstone.view.activity.LoginActivity;
import coursera.capstone.view.adapter.WatchlistListAdapter;
import coursera.capstone.view.control.ImageViewButton;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class WatchlistFragment extends Fragment {
	
	private final static String TAG = WatchlistFragment.class.getSimpleName();
	private final static String FRAGMENT_DATA_TAG = "coursera.capstone.tag.watchlist.data";
	private final static String LIST_TAG = "coursera.capstone.tag.watchlist.list";
	private final static String LIST_DT_TAG = "coursera.capstone.tag.watchlist.dt";
	
	private RetainedDataFragment retainedDataFragment;
	private DiabetesDataSource diabetesDataSource;
	private WatchlistListAdapter watchlistListAdapter;
	private TextView usernameEditText;
	private ListView followersListView;
	private long dataDt;
	private ArrayList<DiabetesFollower> watchlistItems;
	private boolean ignoreUpdate = false;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_followers, container, false);
    }

    @Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		Context context = getActivity().getApplicationContext();
		diabetesDataSource = DiabetesDataSource.get(context.getApplicationContext());
        loadViews((ViewGroup)view);
		retainData();
	}

    @Override
    public void onPause() {
		diabetesDataSource.setFollowerListner(null);
		diabetesDataSource.setFollowersListner(null);
		saveData();		
		super.onPause();
	}

	@Override
	public void onResume() {
		diabetesDataSource.setFollowerListner(followerListner);
		diabetesDataSource.setFollowersListner(followerListListner);
		updateList(false);
		super.onResume();
	}
	
	private void loadViews(ViewGroup parent) {
		ImageViewButton addButton = (ImageViewButton)parent.findViewById(R.id.AddImageViewButton);
		usernameEditText = (TextView)parent.findViewById(R.id.UsernameEditText);
		followersListView = (ListView)parent.findViewById(R.id.FollowersListView);
		watchlistListAdapter = new WatchlistListAdapter(getActivity(), onClickListner);
		followersListView.setAdapter(watchlistListAdapter);
		
		addButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				addToWatchlist();				
			}
		});
	}

	private void retainData() {
		FragmentManager fragmentManager = getFragmentManager();
		retainedDataFragment = (RetainedDataFragment) fragmentManager.findFragmentByTag(FRAGMENT_DATA_TAG);
        if (retainedDataFragment == null) {
        	retainedDataFragment = new RetainedDataFragment();
            fragmentManager.beginTransaction().add(retainedDataFragment, FRAGMENT_DATA_TAG).commit();
            watchlistItems = new ArrayList<DiabetesFollower>();
            retainedDataFragment.addData(LIST_TAG, watchlistItems);
            dataDt = 0;
            retainedDataFragment.addData(LIST_DT_TAG, dataDt);
            if (!diabetesDataSource.isLogined()) {
	            ignoreUpdate = true;
	            startLoginActivity();
            }
        } else {
        	try {
        		watchlistItems = (ArrayList<DiabetesFollower>)retainedDataFragment.getData(LIST_TAG);
        		dataDt = (long)retainedDataFragment.getData(LIST_DT_TAG);
        	} catch(Exception e) {
        		
        	}
        }
	}
	
	private void saveData() {
		FragmentManager fragmentManager = getFragmentManager();
		retainedDataFragment = (RetainedDataFragment) fragmentManager.findFragmentByTag(FRAGMENT_DATA_TAG);
        if (retainedDataFragment != null) {
            retainedDataFragment.addData(LIST_DT_TAG, dataDt);
        }
	}
	
	public void updateList(boolean force) {
		if (ignoreUpdate) {
			ignoreUpdate = false;
			return;
		}
		Log.i(TAG, "List update");
		if (force || (new Date().getTime() - dataDt > DiabetesConstants.LIST_CACHE_TIMEOUT)) {
			LoadingDialog.showLoading(getFragmentManager(), getString(R.string.loading));
			diabetesDataSource.getWatchlist();
		} else {
			watchlistListAdapter.fillItems(watchlistItems);
		}
	}
	
	private View.OnClickListener onClickListner = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			try {
				DiabetesFollower follower = (DiabetesFollower)v.getTag();
				if (null != follower) {
					if (follower.isApproved()) {
						Intent intent = new Intent(getActivity(), FeedbackActivity.class);
						FeedbackActivity.setFollowerData(follower);
						startActivity(intent);
					} else {
						Toast.makeText(getActivity(), R.string.error_watchlist_not_approved, 
								Toast.LENGTH_SHORT).show();
					}
				}
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
		}
	};
	
	private void startLoginActivity() {
		try {
			startActivity(new Intent(getActivity(), LoginActivity.class));
		} catch (Exception e) {
			
		}
	}
	
	private void addToWatchlist() {
		String username = usernameEditText.getText().toString();
		if (username.length() < 5) {
			showError(R.string.error_short_login);
			return;
		}
		LoadingDialog.showLoading(getFragmentManager(), getString(R.string.loading));
		diabetesDataSource.addToWatchlist(username);
	}
	
	protected void showError(int errorTextId) {
		String errorText = getString(errorTextId);
		Log.e(TAG, errorText);
		try {
			Toast.makeText(getActivity(), errorText, Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			
		}		
	}
	
	DiabetesDataSource.FollowerListner followerListner = new DiabetesDataSource.FollowerListner() {
		@Override
		public void onLoadFollower(DiabetesFollower follower) {
			if (null != follower) {
				Log.i(TAG, "onLoadFollower ok");
				usernameEditText.setText("");
				diabetesDataSource.getWatchlist();
			} else {
				LoadingDialog.hideLoading(getFragmentManager());
			}			
		}		
	};
	
	DiabetesDataSource.FollowersListner followerListListner = new DiabetesDataSource.FollowersListner() {
		@Override
		public void onLoadFollowers(Collection<DiabetesFollower> followers) {
			if (null != followers) {
				dataDt = new Date().getTime();
				watchlistItems.clear();
				watchlistItems.addAll(followers);
				watchlistListAdapter.fillItems(watchlistItems);
			}
			LoadingDialog.hideLoading(getFragmentManager());
		}
	};
}
