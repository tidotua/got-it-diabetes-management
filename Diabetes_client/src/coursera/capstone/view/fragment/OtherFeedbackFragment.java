package coursera.capstone.view.fragment;

import java.util.Date;

import coursera.capstone.R;
import coursera.capstone.model.DiabetesFollower;
import coursera.capstone.utils.DiabetesConstants;
import android.os.Bundle;
import android.view.View;

public class OtherFeedbackFragment extends FeedbackFragment {
	
	private final static String TAG = OtherFeedbackFragment.class.getSimpleName();
	private final static String FRAGMENT_DATA_TAG = "coursera.capstone.tag.feedback.data";
	
	private DiabetesFollower followerData;
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (null != feedbackListAdapter && null != followerData) {
			feedbackListAdapter.setFollowerData(followerData);
		}
	}
	
	public void setFollowerData(DiabetesFollower followerData) {
		this.followerData = followerData;
	}

	@Override
	protected String getDataTag() {
		return OtherFeedbackFragment.FRAGMENT_DATA_TAG;
	}
	
	@Override
	protected boolean needUpdate() {
		return (new Date().getTime() - dataDt > DiabetesConstants.LIST_CACHE_TIMEOUT);
	}
	
	@Override
	protected void fetchFeedback() {
		if (null != followerData) {
			LoadingDialog.showLoading(getFragmentManager(), getString(R.string.loading));
			diabetesDataSource.getWatchlistFeedback(followerData.getUid());
		}
	}
}
