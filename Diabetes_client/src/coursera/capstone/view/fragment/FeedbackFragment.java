package coursera.capstone.view.fragment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import coursera.capstone.R;
import coursera.capstone.controller.DiabetesDataSource;
import coursera.capstone.model.CheckInDailyData;
import coursera.capstone.model.CheckInData;
import coursera.capstone.utils.DiabetesConstants;
import coursera.capstone.view.activity.LoginActivity;
import coursera.capstone.view.activity.MainActivity;
import coursera.capstone.view.adapter.FeedbackListAdapter;
import coursera.capstone.view.control.DailyLinearPlotView;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.AbsListView.OnScrollListener;

public class FeedbackFragment extends Fragment {
	
	private final static String TAG = FeedbackFragment.class.getSimpleName();
	private final static String FRAGMENT_DATA_TAG = "coursera.capstone.tag.main.data";
	private final static String LIST_TAG = "coursera.capstone.tag.feedback.checkindata";
	private final static String LIST_DT_TAG = "coursera.capstone.tag.feedback.dt";
	private final static String PLOT_DT_TAG = "coursera.capstone.tag.feedback.plotdt";
	
	protected DiabetesDataSource diabetesDataSource;
	protected FeedbackListAdapter feedbackListAdapter;
	private ListView feedbackListView;
	private DailyLinearPlotView dailyPlotView;
	protected long dataDt;
	private ArrayList<CheckInData> feedbackItems;
	private boolean ignoreUpdate = false;
	private int firstVisibleListItem = 0;
	private View scrollDirector = null;
	private long retainedPlotVisibleTimeStart = 0;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.feedback_fragment, container, false);
    }	

    @Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		Context context = getActivity().getApplicationContext();
		diabetesDataSource = DiabetesDataSource.get(context);
		feedbackListView = (ListView)view.findViewById(R.id.FeedbackListView);
		feedbackListAdapter = new FeedbackListAdapter(context, onClickListner);
		feedbackListView.setAdapter(feedbackListAdapter);
		feedbackListView.setOnScrollListener(scrollListner);
		dailyPlotView = (DailyLinearPlotView)view.findViewById(R.id.DailyPlotView);
		dailyPlotView.setVisibleYBounds(0, DiabetesConstants.MAX_SUGAR);
		dailyPlotView.setCriticalYBounds(DiabetesConstants.LOW_SUGAR, DiabetesConstants.HIGH_SUGAR);
		dailyPlotView.setLinearPlotListner(plotListner);
		retainData();
	}

	@Override
	public void onPause() {
		diabetesDataSource.setFeedbackListner(null);
		saveData();		
		super.onPause();
	}

	@Override
	public void onResume() {
		diabetesDataSource.setFeedbackListner(feedbackListner);
		updateList(false);
		super.onResume();
	}
	
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == MainActivity.CHECKIN_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
			resetScroll();
		}		
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void retainData() {
		FragmentManager fragmentManager = getFragmentManager();
		RetainedDataFragment retainedDataFragment = (RetainedDataFragment) fragmentManager.findFragmentByTag(getDataTag());
        if (retainedDataFragment == null) {
        	retainedDataFragment = new RetainedDataFragment();
            fragmentManager.beginTransaction().add(retainedDataFragment, getDataTag()).commit();
            feedbackItems = new ArrayList<CheckInData>();
            retainedDataFragment.addData(LIST_TAG, feedbackItems);
            dataDt = 0;
            retainedDataFragment.addData(LIST_DT_TAG, dataDt);
            if (!diabetesDataSource.isLogined()) {
	            ignoreUpdate = true;
	            startLoginActivity();
            }
        } else {
        	try {
	        	feedbackItems = (ArrayList<CheckInData>)retainedDataFragment.getData(LIST_TAG);
	        	dataDt = (long)retainedDataFragment.getData(LIST_DT_TAG);
	        	retainedPlotVisibleTimeStart = (long)retainedDataFragment.getData(PLOT_DT_TAG);
        	} catch (Exception e) {
        		Log.e(TAG, "Data retain failed");
        	}
        }
	}
	
	private void saveData() {
		FragmentManager fragmentManager = getFragmentManager();
		RetainedDataFragment retainedDataFragment = (RetainedDataFragment) fragmentManager.findFragmentByTag(getDataTag());
        if (retainedDataFragment != null) {
            retainedDataFragment.addData(LIST_DT_TAG, dataDt);
            retainedPlotVisibleTimeStart = dailyPlotView.getVisibleTimeStart();
            retainedDataFragment.addData(PLOT_DT_TAG, retainedPlotVisibleTimeStart);
        }
	}
	
	public void updateList(boolean force) {
		if (ignoreUpdate) {
			ignoreUpdate = false;
			return;
		}
		Log.i(TAG, "List update");
		if (force || needUpdate()) {
			fetchFeedback();
		} else {
			feedbackListAdapter.fillItems(feedbackItems);
			updatePlot();
			updateLoginState();
		}		
	}
	
	private void updatePlot() {
		dailyPlotView.reset();
		for (CheckInData checkInData: feedbackItems) {
			dailyPlotView.addValue(checkInData.getDateTime(), checkInData.getBloodSugarLevel());
		}
		dailyPlotView.rebuildPlot();
		
		if (0 != retainedPlotVisibleTimeStart) {
			dailyPlotView.scrollPlotToTime(retainedPlotVisibleTimeStart);
			retainedPlotVisibleTimeStart = 0;
		}
	}
	
	public void resetScroll() {
		try {
			retainedPlotVisibleTimeStart = 0;
			dailyPlotView.scrollPlotToTime(new Date().getTime());
			feedbackListView.setSelection(0);
		} catch(Exception e) {
			
		}
	}
	
	private DailyLinearPlotView.LinearPlotListner plotListner = new DailyLinearPlotView.LinearPlotListner() {
		@Override
		public void onScroll(DailyLinearPlotView plotView, long newestVisibleItemTime) {
			if (plotView == scrollDirector) {
				int visibleIndex = feedbackListAdapter.getIndexByTime(newestVisibleItemTime);
				feedbackListView.smoothScrollToPositionFromTop(visibleIndex, 0);
			}			
		}

		@Override
		public void onScrollStateChanged(DailyLinearPlotView plotView,
				int newState) {
			if (null == scrollDirector) {
				if (newState != OnScrollListener.SCROLL_STATE_IDLE) {
					scrollDirector = plotView;
				}				
			} else if (plotView == scrollDirector && newState == OnScrollListener.SCROLL_STATE_IDLE) {
				scrollDirector = null;
			}
		}
	};
	
	private AbsListView.OnScrollListener scrollListner = new AbsListView.OnScrollListener() {
		@Override
		public void onScroll(AbsListView list, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			if (firstVisibleListItem == firstVisibleItem || list != scrollDirector) {
				return;
			}
			firstVisibleListItem = firstVisibleItem;
			CheckInData itemData = (CheckInData)feedbackListAdapter.getItem(firstVisibleListItem);
			if (itemData.getClass().equals(CheckInDailyData.class)) {
				dailyPlotView.smoothScrollToDay(itemData.getDateTime());
			} else {
				dailyPlotView.smoothScrollToLastVisibleTimePoint(itemData.getDateTime());
			}
		}

		@Override
		public void onScrollStateChanged(AbsListView list, int state) {
			if (null == scrollDirector) {
				if (state != OnScrollListener.SCROLL_STATE_IDLE) {
					scrollDirector = list;
				}				
			} else if (list == scrollDirector && state == OnScrollListener.SCROLL_STATE_IDLE) {
				scrollDirector = null;
			}
		}
	};
	
	private DiabetesDataSource.FeedbackListner feedbackListner = new DiabetesDataSource.FeedbackListner() {

		@Override
		public void onLoadFeedbacks(Collection<CheckInData> list) {
			if (null != list) {
				Log.i(TAG, "List received");
				dataDt = new Date().getTime();
				feedbackItems.clear();
				if (diabetesDataSource.isLogined()) {
					feedbackItems.addAll(list);
				}
				feedbackListAdapter.fillItems(feedbackItems);
				updatePlot();
			}
			LoadingDialog.hideLoading(getFragmentManager());
			updateLoginState();	
		}
		
	};

	private View.OnClickListener onClickListner = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			try {
				
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
		}
	};
	
	private void startLoginActivity() {
		new Handler().post(new Runnable(){
            @Override
            public void run() {
            	try {
            		startActivity(new Intent(getActivity(), LoginActivity.class));
        		} catch (Exception e) {
        			
        		}
            }
        });

	}
	
	private void updateLoginState() {
		new Handler().post(new Runnable(){
            @Override
            public void run() {
            	try {
            		((MainActivity)getActivity()).updateLoginState();
        		} catch (Exception e) {
        			
        		}
            }
        });
	}
	
	// To Override
	
	protected String getDataTag() {
		return FeedbackFragment.FRAGMENT_DATA_TAG;
	}
	
	protected boolean needUpdate() {
		return diabetesDataSource.getLastDataChangeDt() > dataDt;
	}
	
	protected void fetchFeedback() {
		LoadingDialog.showLoading(getFragmentManager(), getString(R.string.loading));
		diabetesDataSource.getFeedback();
	}
}
