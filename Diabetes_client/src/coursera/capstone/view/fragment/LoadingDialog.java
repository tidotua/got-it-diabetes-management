package coursera.capstone.view.fragment;

import coursera.capstone.R;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

// DialogFragment retain bug http://stackoverflow.com/questions/12433397/android-dialogfragment-disappears-after-orientation-change

public class LoadingDialog extends DialogFragment {
	
	final private static String LOADING_DIALOG_TAG = "vandy.mooc.tag.loadingdialog";
	
	protected static DialogFragment savedDialogFragment;
	
	private TextView loadingTitle;
	private String title = "";
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setCancelable(false);
    }
	
	@Override
    public void onDestroy() {
        if (this == savedDialogFragment) {
            savedDialogFragment = null;
        }
        super.onDestroy();
    }
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		return inflater.inflate(R.layout.loading_fragment, container, false);
    }
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		loadingTitle = (TextView)view.findViewById(R.id.LoadingTitle);
		loadingTitle.setText(title);
	}

	@Override
	public void onDestroyView() {
	  if (getDialog() != null && getRetainInstance())
	    getDialog().setDismissMessage(null);
	  super.onDestroyView();
	}
	
	public void setTitle(String title) {
		try {
			this.title= title; 
			loadingTitle.setText(title);
		} catch (Exception e) {
			
		}		
	}
	
	public static void showLoading(Activity activity, String title) {
		showLoading(activity.getFragmentManager(), title);
	}
	
	public static void showLoading(FragmentManager fragmentManager, String title) {
		if (null == fragmentManager) {
			return;
		}
		LoadingDialog fragment = (LoadingDialog)fragmentManager.findFragmentByTag(LOADING_DIALOG_TAG);
		if (null == fragment) {
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragment = new LoadingDialog();
			fragment.show(fragmentTransaction, LOADING_DIALOG_TAG);
		}
		fragment.setTitle(title);
		savedDialogFragment = fragment;
	}
	
	public static void hideLoading(Activity activity) {
		hideLoading(activity.getFragmentManager());
	}
	
	public static void hideLoading(FragmentManager fragmentManager) {
		if (null == fragmentManager) {
			return;
		}			
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		Fragment prev = fragmentManager.findFragmentByTag(LOADING_DIALOG_TAG);
		if (prev != null) {
			fragmentTransaction.remove(prev);
		}
		fragmentTransaction.commit();
	}
	
	public static void hideLoading() {
        try {
            if (null != savedDialogFragment) {
                savedDialogFragment.dismissAllowingStateLoss();
            }
        } catch (Exception e) {
            
        }
    }
}
