package coursera.capstone.view.adapter;

import java.util.ArrayList;
import java.util.Collection;

import coursera.capstone.controller.DiabetesDataSource;
import coursera.capstone.model.DiabetesFollower;
import coursera.capstone.view.activity.FollowerActivity;
import coursera.capstone.view.control.ImageViewButton;

import coursera.capstone.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FollowersListAdapter extends BaseAdapter {
	
	protected Context context;
	protected View.OnClickListener onClickListner;
	protected ArrayList<DiabetesFollower> followerItems = new ArrayList<DiabetesFollower>();
	
	public FollowersListAdapter(Context context, View.OnClickListener onClickListner) {
		this.context = context;
		this.onClickListner = onClickListner;
	}
	
	@Override
	public int getCount() {
		return followerItems.size();
	}

	@Override
	public Object getItem(int i) {
        if (i > -1 && i < getCount() )
            return followerItems.get(i);
		return null;
	}

	@Override
	public long getItemId(int i) {
		DiabetesFollower followerItem = (DiabetesFollower)getItem(i);
        if (null != followerItem)
            return followerItem.getId();
		return 0;
	}

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewGroup itemView = (ViewGroup)view;
        if (null == itemView) {
            LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = (ViewGroup)layoutInflater.inflate(R.layout.follower_list_item, null);
        }
        itemView.setOnClickListener(onClickListner);
        setContentToView(itemView, i);
        return itemView;
    }

    protected void setContentToView(ViewGroup itemViewGroup, int index)
    {
    	final DiabetesFollower followerItem = (DiabetesFollower)getItem(index);
        if (null == followerItem) {
    		return;
    	}
        itemViewGroup.setTag(followerItem);
        itemViewGroup.setBackgroundResource(index % 2 == 0 ? R.drawable.item_selection_color_1 : R.drawable.item_selection_color_2);
        
    	TextView fullnameTextView = (TextView)itemViewGroup.findViewById(R.id.FullNameTextView);
    	fullnameTextView.setText(followerItem.getFullName());
    	
    	ImageViewButton approveImageViewButton = (ImageViewButton)itemViewGroup.findViewById(R.id.ApproveImageViewButton);
    	ImageViewButton editImageViewButton = (ImageViewButton)itemViewGroup.findViewById(R.id.EditImageViewButton);
    	ImageViewButton removeImageViewButton = (ImageViewButton)itemViewGroup.findViewById(R.id.RemoveImageViewButton);
    	
    	approveImageViewButton.setVisibility(followerItem.isApproved() ? View.GONE : View.VISIBLE);
    	editImageViewButton.setVisibility(followerItem.isApproved() ? View.VISIBLE : View.GONE);
    	
    	approveImageViewButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				approveFollower(followerItem);
			}
    	});
    	
    	editImageViewButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				showEditActivity(followerItem);
			}
    	});
    	
    	removeImageViewButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				showRemoveDialog(followerItem);
			}
    	});
    }
    
    public void fillItems(Collection<DiabetesFollower> items)
    {
    	followerItems.clear();
    	followerItems.addAll(items);
        notifyDataSetChanged();
    }
    
    private void showRemoveDialog(final DiabetesFollower followerItem) {
		new AlertDialog.Builder(context)
                .setTitle(R.string.remove_user_dialog_title)
                .setMessage(String.format(context.getString(R.string.remove_user_dialog_text), followerItem.getFullName()))
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    	DiabetesDataSource.get(context.getApplicationContext()).removeFollower(followerItem);
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    
    private void approveFollower(DiabetesFollower followerItem) {
    	followerItem.setApproved(true);
    	DiabetesDataSource.get(context.getApplicationContext()).setFollower(followerItem);
    }
    
    private void showEditActivity(DiabetesFollower followerItem) {
    	FollowerActivity.setFollowerItem(followerItem);
    	context.startActivity(new Intent(context, FollowerActivity.class));
    }
}
