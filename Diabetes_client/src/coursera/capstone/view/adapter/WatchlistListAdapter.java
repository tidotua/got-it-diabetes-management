package coursera.capstone.view.adapter;

import coursera.capstone.model.DiabetesFollower;
import coursera.capstone.view.control.ImageViewButton;

import coursera.capstone.R;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

public class WatchlistListAdapter extends FollowersListAdapter {
	
    public WatchlistListAdapter(Context context, OnClickListener onClickListner) {
		super(context, onClickListner);
	}
    
    @Override
	protected void setContentToView(ViewGroup itemViewGroup, int index)
    {
    	super.setContentToView(itemViewGroup, index);
    	
    	final DiabetesFollower followerItem = (DiabetesFollower)getItem(index);
        if (null == followerItem) {
    		return;
    	}
    	
        ImageView waitImageView = (ImageView)itemViewGroup.findViewById(R.id.PendingImageView);
		ImageViewButton approveImageViewButton = (ImageViewButton)itemViewGroup.findViewById(R.id.ApproveImageViewButton);
    	ImageViewButton editImageViewButton = (ImageViewButton)itemViewGroup.findViewById(R.id.EditImageViewButton);
    	
    	waitImageView.setVisibility(followerItem.isApproved() ? View.GONE : View.VISIBLE);
    	approveImageViewButton.setVisibility(View.GONE);
    	editImageViewButton.setVisibility(View.GONE);
    }

}
