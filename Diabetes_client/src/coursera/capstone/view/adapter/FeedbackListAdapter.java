package coursera.capstone.view.adapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import coursera.capstone.controller.DiabetesNotificationManager;
import coursera.capstone.model.CheckInDailyData;
import coursera.capstone.model.CheckInData;
import coursera.capstone.model.DiabetesFollower;
import coursera.capstone.utils.DiabetesConstants;

import coursera.capstone.R;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FeedbackListAdapter extends BaseAdapter {
	
	private Context context;
	private View.OnClickListener onClickListner;
	private ArrayList<CheckInData> feedbackItems = new ArrayList<CheckInData>();
	private int normalTextColor;
	private int alarmTextColor;
	private DiabetesFollower followerData;
	
	public FeedbackListAdapter(Context context, View.OnClickListener onClickListner) {
		this.context = context;
		this.onClickListner = onClickListner;
		normalTextColor = context.getResources().getColor(R.color.normal_text);
        alarmTextColor = context.getResources().getColor(R.color.alarm_text);
	}
	
	@Override
	public int getCount() {
		return feedbackItems.size();
	}

	@Override
	public Object getItem(int i) {
        if (i > -1 && i < getCount() )
            return feedbackItems.get(i);
		return null;
	}

	@Override
	public long getItemId(int i) {
		CheckInData feedbackItem = (CheckInData)getItem(i);
        if (null != feedbackItem)
            return feedbackItem.getId();
		return 0;
	}

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewGroup itemView = (ViewGroup)view;
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        CheckInData feedbackItem = (CheckInData)getItem(i);
    	if (null == feedbackItem) {
    		return itemView;
    	}
    	
    	if (feedbackItem.getClass().equals(CheckInDailyData.class)) {
    		itemView = (ViewGroup)layoutInflater.inflate(R.layout.feedback_daily_list_item, null);
    		setContentToDailyView(itemView, i, feedbackItem);
    	} else {
    		itemView = (ViewGroup)layoutInflater.inflate(R.layout.feedback_list_item, null);
    		setContentToItemView(itemView, i, feedbackItem);
    	}
        return itemView;
    }
    
    public int getIndexByTime(long timeMs) {
    	int i = 0;
    	boolean priorDay = false;
    	for (CheckInData item: feedbackItems) {
    		if (item.getClass().equals(CheckInDailyData.class)) {
    			priorDay = true;
    		} else {
    			if (item.getDateTime() == timeMs) {
    				return priorDay ? i - 1 : i;
    			}
    			priorDay = false;
    		}
    		++i;
    	}
    	return -1;
    }
    
    private void setContentToDailyView(ViewGroup itemViewGroup, int index, CheckInData feedbackItem) {
    	TextView dateTimeTextView = (TextView)itemViewGroup.findViewById(R.id.DateTimeTextView);
    	TextView bloodSugarLevelTextView = (TextView)itemViewGroup.findViewById(R.id.BloodSugarLevelTextView);
    	
    	Date date = new Date(feedbackItem.getDateTime());
    	String dateText = DiabetesConstants.dateFormater.format(date);
    	dateTimeTextView.setText(dateText);
		
    	updateBloodSugarLevel(bloodSugarLevelTextView, feedbackItem.getBloodSugarLevel());
    }

    private void setContentToItemView(ViewGroup itemViewGroup, int index, CheckInData feedbackItem)
    {
    	itemViewGroup.setTag(feedbackItem);
    	itemViewGroup.setBackgroundColor(context.getResources().getColor(index % 2 == 0 ? R.color.color_back_1 : R.color.color_back_2));
    	    	
    	TextView dateTimeTextView = (TextView)itemViewGroup.findViewById(R.id.DateTimeTextView);
    	ImageView moodImageView = (ImageView)itemViewGroup.findViewById(R.id.MoodImageView);
    	ImageView administerInsulineImageView = (ImageView)itemViewGroup.findViewById(R.id.AdministerInsulineImageView);
    	TextView bloodSugarLevelTextView = (TextView)itemViewGroup.findViewById(R.id.BloodSugarLevelTextView);
    	ViewGroup mealViewGroup = (ViewGroup)itemViewGroup.findViewById(R.id.MealViewGroup);
    	TextView mealTextView = (TextView)itemViewGroup.findViewById(R.id.MealTextView);
    	
    	Date date = new Date(feedbackItem.getDateTime());
    	String dateText = DiabetesConstants.timeFormater.format(date);
    	dateTimeTextView.setText(dateText);
    	
    	if (null != followerData && !followerData.isShowAdministerInsuline()) {
    		administerInsulineImageView.setVisibility(View.GONE);
    	} else {
    		administerInsulineImageView.setVisibility(feedbackItem.isAdministerInsulin() ? View.VISIBLE: View.INVISIBLE);
    	}
    	
    	if (null != followerData && !followerData.isShowMood()) {
    		moodImageView.setVisibility(View.GONE);
    	} else {
    		moodImageView.setVisibility(View.VISIBLE);
    		moodImageView.setImageResource(DiabetesConstants.MOOD_IMAGES[feedbackItem.getMood()]);
    		updateSmileColor(moodImageView, feedbackItem.getMood());
    	}
    	
    	if (null != followerData && !followerData.isShowMeal() || feedbackItem.getMealText().isEmpty()) {
    		mealViewGroup.setVisibility(View.GONE);
    	} else {
    		mealViewGroup.setVisibility(View.VISIBLE);
    		mealTextView.setText(feedbackItem.getMealText());
    	}
		
		updateBloodSugarLevel(bloodSugarLevelTextView, feedbackItem.getBloodSugarLevel());
    }
    
    private void updateBloodSugarLevel(TextView bloodSugarLevelTextView, int sugarLevel) {
		int color = (sugarLevel >= DiabetesConstants.LOW_SUGAR && sugarLevel <= DiabetesConstants.HIGH_SUGAR) ? 
				normalTextColor : alarmTextColor;
		String text = context.getString(R.string.sugar_normal);
		if (sugarLevel < DiabetesConstants.LOW_SUGAR) {
			text = context.getString(R.string.sugar_low);
		} else if (sugarLevel > DiabetesConstants.HIGH_SUGAR) {
			text = context.getString(R.string.sugar_high);
		}
		bloodSugarLevelTextView.setText(String.format("%d (%s)", sugarLevel, text));
		bloodSugarLevelTextView.setTextColor(color);
	}
    
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	private void updateSmileColor(ImageView smileImageView, int mood) {
		int color = 0;
		if (mood > DiabetesConstants.NORMAL_MOOD) {
			color = context.getResources().getColor(R.color.mood_good);
		} else if (mood < DiabetesConstants.NORMAL_MOOD) {
			color = context.getResources().getColor(R.color.mood_bad);
		} else if (DiabetesConstants.NORMAL_MOOD == mood) {
			smileImageView.clearColorFilter();
			return;
		}
        PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN);
        smileImageView.setColorFilter(colorFilter);
	}
	
    public void setFollowerData(DiabetesFollower followerData) {
		this.followerData = followerData;
	}
    
    public void fillItems(Collection<CheckInData> items)
    {
    	feedbackItems.clear();
    	
    	CheckInDailyData dailyItem = null;
    	int dailyEventCounter = 0;
    	int sugarDailySum = 0;
    	int moodDailySum = 0;
    	for (CheckInData item: items) {
    		long itemDate = DiabetesNotificationManager.getDateHour(item.getDateTime(), 0);
    		if (dailyItem == null || itemDate != dailyItem.getDateTime()) {
    			dailyItem = new CheckInDailyData();
    			dailyItem.setDateTime(itemDate);
    			dailyEventCounter = 0;
    			sugarDailySum = 0;
    			moodDailySum = 0;
    			feedbackItems.add(dailyItem);
    		}
    		dailyEventCounter += 1;
    		sugarDailySum += item.getBloodSugarLevel();
    		moodDailySum += item.getMood();
    		dailyItem.setBloodSugarLevel((short)(sugarDailySum / dailyEventCounter));
			dailyItem.setMood((short)(moodDailySum / dailyEventCounter));
			feedbackItems.add(item);
    	}
    	
        notifyDataSetChanged();
    }
    
    
}
