package coursera.capstone.view.activity;

import coursera.capstone.controller.DiabetesDataSource;
import coursera.capstone.controller.DiabetesNotificationManager;
import coursera.capstone.controller.UserPersistanceManager;
import coursera.capstone.model.DiabetesUser;
import coursera.capstone.view.fragment.LoadingDialog;

import coursera.capstone.R;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity {
	
	private final static String TAG = LoginActivity.class.getSimpleName();
	
	private DiabetesDataSource diabetesDataSource;
	
	private EditText loginEditText;
	private EditText passwordEditText;
	private TextView registerTextView;
	private CheckBox rememberMeCheckBox;
	private Button loginButton;
	private boolean autoLogin = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		diabetesDataSource = DiabetesDataSource.get(this.getApplicationContext());
		
		ActionBar actionBar = getActionBar();
        if (null != actionBar) {
            actionBar.setDisplayHomeAsUpEnabled(true);            
        }
		
		loginEditText = (EditText)this.findViewById(R.id.LoginEditText);
		passwordEditText = (EditText)this.findViewById(R.id.PasswordEditText);
		rememberMeCheckBox = (CheckBox)this.findViewById(R.id.RememberMeCheckBox);
		loginButton = (Button)this.findViewById(R.id.LoginButton);
		registerTextView = (TextView)this.findViewById(R.id.RegisterTextView);
		
		loginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				login();
			}
		});
		
		registerTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				register();
			}
		});
		
		loadCredentials();
	}
	
	@Override
	protected void onPause() {
		diabetesDataSource.setProfileLoadListner(null);
		super.onPause();
	}

	@Override
	protected void onResume() {
		diabetesDataSource.setProfileLoadListner(profileLoadListner);
		super.onResume();
	}
	
	@Override
	public void onBackPressed() {
	  finish();
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	
	private void login() {
		String login = loginEditText.getText().toString();
		String pass = passwordEditText.getText().toString();
		
		if (login.length() < 5) {
			showError(R.string.error_short_login);
			return;
		}
		
		if (pass.length() < 4) {
			showError(R.string.error_short_password);
			return;
		}
		
		LoadingDialog.showLoading(this, getString(R.string.logining));
		diabetesDataSource.login(login, pass);
	}
	
	private void showError(int errorTextId) {
		String errorText = getString(errorTextId);
		Log.e(TAG, errorText);
		Toast.makeText(this, errorText, Toast.LENGTH_SHORT).show();
	}
	
	private void register() {
		startActivity(new Intent(this, RegisterActivity.class));
	}
	
	private void loadCredentials() {
		UserPersistanceManager.Credentials credentials = new UserPersistanceManager().load(this);
		autoLogin = null != credentials;
		if (autoLogin) {
			loginEditText.setText(credentials.login);
			passwordEditText.setText(credentials.password);
			rememberMeCheckBox.setChecked(true);
			login();
		}
	}
	
	private void saveCredentials() {
		if (rememberMeCheckBox.isChecked() && !autoLogin) {
			String login = loginEditText.getText().toString();
			String pass = passwordEditText.getText().toString();
			new UserPersistanceManager().save(LoginActivity.this, login, pass);
		}
	}
	
	DiabetesDataSource.ProfileLoadListner profileLoadListner = new DiabetesDataSource.ProfileLoadListner() {

		@Override
		public void onLoadProfile(DiabetesUser diabetesUser) {
			try {
				LoadingDialog.hideLoading(LoginActivity.this);
				if (null != diabetesUser) {
					saveCredentials();
					if (diabetesUser.getType() == DiabetesUser.UserType.USER_TYPE_TEEN) {
						DiabetesNotificationManager.setSettings(LoginActivity.this,
								diabetesUser.getId(),
								diabetesUser.getNotificationPeriodMs(),
								diabetesUser.isNotificationIgnoreNight());
					} else {
						DiabetesNotificationManager.resetSettings(LoginActivity.this);
					}
					LoginActivity.this.finish();
				}
			} catch (Exception e) {
				
			}
		}
    	
    };
}
