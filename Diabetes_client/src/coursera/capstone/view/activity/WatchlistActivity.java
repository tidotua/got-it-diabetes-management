package coursera.capstone.view.activity;

import coursera.capstone.view.fragment.WatchlistFragment;

import coursera.capstone.R;
import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.MenuItem;

public class WatchlistActivity extends Activity {
	
	private final static String TAG = WatchlistActivity.class.getSimpleName();
	
	private final static String MAIN_FRAGMENT_TAG = "coursera.capstone.tag.watchlist";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_watchlist);
		
		ActionBar actionBar = getActionBar();
        if (null != actionBar) {
            actionBar.setDisplayHomeAsUpEnabled(true);            
        }
        
		setFragment();
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	
	@Override
	public void onBackPressed() {
	  finish();
	}

	private void setFragment() {
		FragmentManager fragmentManager = getFragmentManager();
		WatchlistFragment fragment = (WatchlistFragment)fragmentManager.findFragmentByTag(MAIN_FRAGMENT_TAG);
		if (null != fragment) {
			return;
		}
		fragment = new WatchlistFragment();
		
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.add(R.id.MainContainer, fragment, MAIN_FRAGMENT_TAG);
        fragmentTransaction.commit(); 
	}

}
