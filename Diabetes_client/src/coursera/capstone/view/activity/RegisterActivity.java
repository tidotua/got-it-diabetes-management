package coursera.capstone.view.activity;

import coursera.capstone.model.DiabetesUser;
import coursera.capstone.view.fragment.LoadingDialog;

import coursera.capstone.R;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends UserBaseActivity {
	
	private final static String TAG = RegisterActivity.class.getSimpleName();
	
	private EditText loginEditText;
	private EditText passwordEditText;
	private EditText passwordRepeatEditText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void initView() {
		setContentView(R.layout.activity_register);
	}
	
	@Override
	protected void loadCustomViews() {
		loginEditText = (EditText)this.findViewById(R.id.LoginEditText);
		passwordEditText = (EditText)this.findViewById(R.id.PasswordEditText);
		passwordRepeatEditText = (EditText)this.findViewById(R.id.PasswordRepeatEditText);
		
		Button registerButton = (Button)this.findViewById(R.id.SigninButton);
		registerButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				register();
			}
		});
	}
	
	private void register() {
		DiabetesUser user = readFields();
		if (null != user) {
			LoadingDialog.showLoading(this, getString(R.string.registering));
			diabetesDataSource.register(user);
		}
	}
	
	@Override
	protected DiabetesUser readFields() {
		String login = loginEditText.getText().toString();
		String pass = passwordEditText.getText().toString();
		String pass2 = passwordRepeatEditText.getText().toString();
		
		if (login.length() < 5) {
			showError(R.string.error_short_login);
			return null;
		}
		
		if (pass.length() < 4) {
			showError(R.string.error_short_password);
			return null;
		}
		
		if (!pass.equals(pass2)) {
			showError(R.string.error_passwords_math);
			return null;
		}
		
		DiabetesUser user = super.readFields();
		if (null != user) {
			user.setUsername(login);
			user.setPassword(pass);
		}		
		return user;
	}
    
    @Override
    protected void onLoadComplete(DiabetesUser diabetesUser) {
    	LoadingDialog.hideLoading(RegisterActivity.this);
		if (null != diabetesUser) {
			Toast.makeText(RegisterActivity.this, R.string.registered, Toast.LENGTH_SHORT).show();
			RegisterActivity.this.finish();
		}
    }

}
