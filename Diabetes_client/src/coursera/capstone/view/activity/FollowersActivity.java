package coursera.capstone.view.activity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import coursera.capstone.controller.DiabetesDataSource;
import coursera.capstone.model.DiabetesFollower;
import coursera.capstone.utils.DiabetesConstants;
import coursera.capstone.view.adapter.FollowersListAdapter;
import coursera.capstone.view.control.ImageViewButton;
import coursera.capstone.view.fragment.LoadingDialog;
import coursera.capstone.view.fragment.RetainedDataFragment;

import coursera.capstone.R;
import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FollowersActivity extends Activity {
	
	private final static String TAG = FollowersActivity.class.getSimpleName();
	private final static String FRAGMENT_DATA_TAG = "coursera.capstone.tag.followers.data";
	private final static String LIST_TAG = "coursera.capstone.tag.followers.list";
	private final static String LIST_DT_TAG = "coursera.capstone.tag.followers.dt";
	
	private RetainedDataFragment retainedDataFragment;
	private DiabetesDataSource diabetesDataSource;
	private FollowersListAdapter followersListAdapter;
	private TextView usernameEditText;
	private ListView followersListView;
	private long dataDt;
	private ArrayList<DiabetesFollower> followerItems;
	private boolean ignoreUpdate = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_followers);
		
		ActionBar actionBar = getActionBar();
        if (null != actionBar) {
            actionBar.setDisplayHomeAsUpEnabled(true);            
        }
        
		diabetesDataSource = DiabetesDataSource.get(this.getApplicationContext());
        loadViews();
		retainData();
	}
	
	@Override
	protected void onPause() {
		diabetesDataSource.setFollowerListner(null);
		diabetesDataSource.setFollowersListner(null);
		saveData();		
		super.onPause();
	}

	@Override
	protected void onResume() {
		diabetesDataSource.setFollowerListner(followerListner);
		diabetesDataSource.setFollowersListner(followerListListner);
		updateList(false);
		super.onResume();
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	
	@Override
	public void onBackPressed() {
	  finish();
	}
	
	private void loadViews() {
		ImageViewButton addButton = (ImageViewButton)this.findViewById(R.id.AddImageViewButton);
		usernameEditText = (TextView)this.findViewById(R.id.UsernameEditText);
		followersListView = (ListView)this.findViewById(R.id.FollowersListView);
		followersListAdapter = new FollowersListAdapter(this, onClickListner);
		followersListView.setAdapter(followersListAdapter);
		
		addButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				addFollower();				
			}
		});
	}

	private void retainData() {
		FragmentManager fragmentManager = getFragmentManager();
		retainedDataFragment = (RetainedDataFragment) fragmentManager.findFragmentByTag(FRAGMENT_DATA_TAG);
        if (retainedDataFragment == null) {
        	retainedDataFragment = new RetainedDataFragment();
            fragmentManager.beginTransaction().add(retainedDataFragment, FRAGMENT_DATA_TAG).commit();
            followerItems = new ArrayList<DiabetesFollower>();
            retainedDataFragment.addData(LIST_TAG, followerItems);
            dataDt = 0;
            retainedDataFragment.addData(LIST_DT_TAG, dataDt);
            if (!diabetesDataSource.isLogined()) {
	            ignoreUpdate = true;
	            startLoginActivity();
            }
        } else {
        	try {
        		followerItems = (ArrayList<DiabetesFollower>)retainedDataFragment.getData(LIST_TAG);
        		dataDt = (long)retainedDataFragment.getData(LIST_DT_TAG);
        	} catch(Exception e) {
        		
        	}
        }
	}
	
	private void saveData() {
		FragmentManager fragmentManager = getFragmentManager();
		retainedDataFragment = (RetainedDataFragment) fragmentManager.findFragmentByTag(FRAGMENT_DATA_TAG);
        if (retainedDataFragment != null) {
            retainedDataFragment.addData(LIST_DT_TAG, dataDt);
        }
	}
	
	public void updateList(boolean force) {
		if (ignoreUpdate) {
			ignoreUpdate = false;
			return;
		}
		Log.i(TAG, "List update");
		if (force || (new Date().getTime() - dataDt > DiabetesConstants.LIST_CACHE_TIMEOUT)) {
			LoadingDialog.showLoading(this, getString(R.string.loading));
			diabetesDataSource.getFollowers();
		} else {
			followersListAdapter.fillItems(followerItems);
		}
	}
	
	private View.OnClickListener onClickListner = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			try {
				
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
		}
	};
	
	private void startLoginActivity() {
		startActivity(new Intent(this, LoginActivity.class));
	}
	
	private void addFollower() {
		String username = usernameEditText.getText().toString();
		if (username.length() < 5) {
			showError(R.string.error_short_login);
			return;
		}
		LoadingDialog.showLoading(this, getString(R.string.loading));
		diabetesDataSource.addFollower(username);
	}
	
	protected void showError(int errorTextId) {
		String errorText = getString(errorTextId);
		Log.e(TAG, errorText);
		Toast.makeText(this, errorText, Toast.LENGTH_SHORT).show();
	}
	
	DiabetesDataSource.FollowerListner followerListner = new DiabetesDataSource.FollowerListner() {
		@Override
		public void onLoadFollower(DiabetesFollower follower) {
			if (null != follower) {
				Log.i(TAG, "onLoadFollower ok");
				usernameEditText.setText("");
				diabetesDataSource.getFollowers();
			} else {
				LoadingDialog.hideLoading(FollowersActivity.this);
			}			
		}		
	};
	
	DiabetesDataSource.FollowersListner followerListListner = new DiabetesDataSource.FollowersListner() {
		@Override
		public void onLoadFollowers(Collection<DiabetesFollower> followers) {
			if (null != followers) {
				dataDt = new Date().getTime();
				followerItems.clear();
				followerItems.addAll(followers);
				followersListAdapter.fillItems(followerItems);
			}
			LoadingDialog.hideLoading(FollowersActivity.this);
		}
	};
}
