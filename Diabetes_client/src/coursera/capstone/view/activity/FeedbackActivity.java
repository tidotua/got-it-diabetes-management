package coursera.capstone.view.activity;

import coursera.capstone.model.DiabetesFollower;
import coursera.capstone.view.fragment.OtherFeedbackFragment;

import coursera.capstone.R;
import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

public class FeedbackActivity extends Activity {
	
	private final static String TAG = FeedbackActivity.class.getSimpleName();
	private final static String MAIN_FRAGMENT_TAG = "coursera.capstone.tag.feedback";
	
	private TextView fullNameTextView;
	private static DiabetesFollower followerData;
	
	public static void setFollowerData(DiabetesFollower followerData) {
		FeedbackActivity.followerData = followerData;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedback);
		
		ActionBar actionBar = getActionBar();
        if (null != actionBar) {
            actionBar.setDisplayHomeAsUpEnabled(true);            
        }
        
		fullNameTextView = (TextView)this.findViewById(R.id.FullNameTextView);
		
		if (null != followerData) {
			fullNameTextView.setText(followerData.getFullName());
			setFragment();
		} else {
			finish();
		}
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	
	@Override
	public void onBackPressed() {
	  finish();
	}

	private void setFragment() {
		FragmentManager fragmentManager = getFragmentManager();
		OtherFeedbackFragment fragment = (OtherFeedbackFragment)fragmentManager.findFragmentByTag(MAIN_FRAGMENT_TAG);
		if (null != fragment) {
			fragment.setFollowerData(followerData);
			return;
		}
		fragment = new OtherFeedbackFragment();
		fragment.setFollowerData(followerData);
		
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.add(R.id.MainContainer, fragment, MAIN_FRAGMENT_TAG);
        fragmentTransaction.commit(); 
	}

}
