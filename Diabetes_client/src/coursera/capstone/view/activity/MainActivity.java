package coursera.capstone.view.activity;

import coursera.capstone.controller.DiabetesDataSource;
import coursera.capstone.controller.DiabetesNotificationManager;
import coursera.capstone.controller.UserPersistanceManager;
import coursera.capstone.model.DiabetesUser;
import coursera.capstone.view.fragment.FeedbackFragment;
import coursera.capstone.view.fragment.WatchlistFragment;

import coursera.capstone.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	private final static String TAG = MainActivity.class.getSimpleName();
	private final static String MAIN_FRAGMENT_TAG = "coursera.capstone.tag.main";
	public final static String FLAG_START_CHECKIN = "coursera.capstone.flag.checkin";
	public final static int CHECKIN_REQUEST_CODE = 9015;
	
    private DiabetesDataSource diabetesDataSource;
	private TextView headerText;
	private Menu menu;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		diabetesDataSource = DiabetesDataSource.get(this.getApplicationContext());
		headerText = (TextView)findViewById(R.id.HeaderText);
		
		if (tryCheckIn(getIntent())) {
			return;
		}
		
		if (!diabetesDataSource.isLogined() && null == savedInstanceState) {
			Log.i(TAG, "Sign in required");
            startLoginActivity();
        }
	}
	
	@Override
	public void onNewIntent(Intent intent){
		super.onNewIntent(intent);
		tryCheckIn(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();
		updateLoginState();
	}
	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Fragment mainFragment = getFragmentManager().findFragmentByTag(MAIN_FRAGMENT_TAG);
		if (null != mainFragment) {
			mainFragment.onActivityResult(requestCode, resultCode, data);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		this.menu = menu;
		updateLoginState();
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.profileAction) {
			startActivity(new Intent(this, EditProfileActivity.class));
			return true;
		} else if (id == R.id.checkInAction) {
			startCheckInActivity();
			return true;
		} else if (id == R.id.watchlistAction) {
			startActivity(new Intent(this, WatchlistActivity.class));
			return true;
		} else if (id == R.id.followersAction) {
			startActivity(new Intent(this, FollowersActivity.class));
			return true;
		} else if (id == R.id.securityAction) {
			if (!diabetesDataSource.isLogined()) {
				startLoginActivity();
			} else {
				showLogoutDialog();
			}
			return true;
		}		
		
		return super.onOptionsItemSelected(item);
	}
	
	public void startLoginActivity() {
		startActivity(new Intent(this, LoginActivity.class));
	}
	
	public void startCheckInActivity() {
		startActivityForResult(new Intent(this, CheckInActivity.class), CHECKIN_REQUEST_CODE);
	}
	
	public boolean tryCheckIn(Intent intent) {
		if (null != intent && intent.getBooleanExtra(FLAG_START_CHECKIN, false)) {
			Log.i(TAG, "Start CheckIn");
			startCheckInActivity();
			return true;
		}
		return false;
	}
	
	private void showLogoutDialog() {
		new AlertDialog.Builder(this)
                .setTitle(R.string.logout_dialog_title)
                .setMessage(R.string.logout_dialog_text)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    	DiabetesNotificationManager.resetSettings(MainActivity.this);
                    	new UserPersistanceManager().clear(MainActivity.this);
                    	Fragment mainFragment = getFragmentManager().findFragmentByTag(MAIN_FRAGMENT_TAG);
                		if (null != mainFragment && mainFragment.getClass().equals(FeedbackFragment.class)) {
                			((FeedbackFragment)mainFragment).resetScroll();
                		}
                		diabetesDataSource.logout();
                    	updateLoginState();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
	
	public void updateLoginState() {
		if (null != menu) {
			updateMenu(menu);
		}
		
		if (diabetesDataSource.isLogined()) {
			headerText.setText(String.format(getString(R.string.authorized), diabetesDataSource.getUserName()));
			setFragment();
		} else {
			headerText.setText(getString(R.string.not_authorized));
			removeFragment();
		}
	}
	
	private void setFragment() {
		FragmentManager fragmentManager = getFragmentManager();
		Fragment oldFragment = fragmentManager.findFragmentByTag(MAIN_FRAGMENT_TAG);
		Fragment newFragment = null;
		
		switch(diabetesDataSource.getUserType()) {
			case USER_TYPE_TEEN:
				if (null != oldFragment && oldFragment.getClass().equals(FeedbackFragment.class)) {
					return;
				}
				newFragment = new FeedbackFragment();
				break;
				
			case USER_TYPE_FOLLOWER:
				if (null != oldFragment && oldFragment.getClass().equals(WatchlistFragment.class)) {
					return;
				}
				newFragment = new WatchlistFragment();
				break;
		};
		
		if (null == newFragment && null == oldFragment) {
			return;
		}
		
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		if (null == newFragment) {
			fragmentTransaction.remove(oldFragment);
		} else if (null == oldFragment) {
            fragmentTransaction.add(R.id.MainContainer, newFragment, MAIN_FRAGMENT_TAG);
        } else {
            fragmentTransaction.replace(R.id.MainContainer, newFragment, MAIN_FRAGMENT_TAG);
        }
        fragmentTransaction.commit();
	}
	
	private void removeFragment() {
		FragmentManager fragmentManager = getFragmentManager();
		Fragment oldFragment = fragmentManager.findFragmentByTag(MAIN_FRAGMENT_TAG);
		if (null != oldFragment) {
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.remove(oldFragment);
			fragmentTransaction.commit();
		}
	}
	
	private void updateMenu(Menu menu) {
		boolean isLogined = diabetesDataSource.isLogined();
		boolean isTeen = diabetesDataSource.getUserType() == DiabetesUser.UserType.USER_TYPE_TEEN;
		
		MenuItem security = menu.findItem(R.id.securityAction);
		security.setIcon(isLogined ? R.drawable.ic_action_secure : R.drawable.ic_action_unsecure);
		security.setTitle(isLogined ? R.string.logout_button : R.string.login_button);
		
		menu.findItem(R.id.checkInAction).setVisible(isLogined && isTeen);
		menu.findItem(R.id.watchlistAction).setVisible(isLogined && isTeen);
		menu.findItem(R.id.followersAction).setVisible(isLogined && isTeen);
		menu.findItem(R.id.profileAction).setVisible(isLogined);
	}
	
}
