package coursera.capstone.view.activity;

import coursera.capstone.controller.DiabetesDataSource;
import coursera.capstone.model.DiabetesFollower;
import coursera.capstone.view.fragment.LoadingDialog;

import coursera.capstone.R;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class FollowerActivity extends Activity {
	
	private final static String TAG = FollowerActivity.class.getSimpleName();
	
	private static DiabetesFollower followerItem;
	
	private DiabetesDataSource diabetesDataSource;
	
	private TextView fullNameTextView;
	private CheckBox shareMoodCheckBox;
	private CheckBox shareMealCheckBox;
	private CheckBox shareAdministerInsulineCheckBox;
	private Button saveButton;
	
	public static void setFollowerItem(DiabetesFollower followerItem) {
		FollowerActivity.followerItem = followerItem;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_follower);
		diabetesDataSource = DiabetesDataSource.get(this.getApplicationContext());
		
		ActionBar actionBar = getActionBar();
        if (null != actionBar) {
            actionBar.setDisplayHomeAsUpEnabled(true);            
        }
		
		if (null == followerItem) {
			finish();
		}
		loadViews();
	}
	
	@Override
	protected void onPause() {
		diabetesDataSource.setFollowerListner(null);
		super.onPause();
	}

	@Override
	protected void onResume() {
		diabetesDataSource.setFollowerListner(followerListner);
		super.onResume();
	}
	
	@Override
	public void onBackPressed() {
	  finish();
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	
	private void loadViews() {
		fullNameTextView = (TextView)this.findViewById(R.id.FullNameTextView);
		shareMoodCheckBox = (CheckBox)this.findViewById(R.id.ShareMoodCheckBox);
		shareMealCheckBox = (CheckBox)this.findViewById(R.id.ShareMealCheckBox);
		shareAdministerInsulineCheckBox = (CheckBox)this.findViewById(R.id.ShareAdministerInsulineCheckBox);
		saveButton = (Button)this.findViewById(R.id.SaveButton);
		
		saveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				save();
			}
		});
		
		fullNameTextView.setText(followerItem.getFullName());
		shareMoodCheckBox.setChecked(followerItem.isShowMood());
		shareMealCheckBox.setChecked(followerItem.isShowMeal());
		shareAdministerInsulineCheckBox.setChecked(followerItem.isShowAdministerInsuline());
	}
	
	private void save() {
		LoadingDialog.showLoading(this, getString(R.string.loading));
		followerItem.setApproved(true);
		followerItem.setShowMood(shareMoodCheckBox.isChecked());
		followerItem.setShowMeal(shareMealCheckBox.isChecked());
		followerItem.setShowAdministerInsuline(shareAdministerInsulineCheckBox.isChecked());
		diabetesDataSource.setFollower(followerItem);
	}

    DiabetesDataSource.FollowerListner followerListner = new DiabetesDataSource.FollowerListner() {
		@Override
		public void onLoadFollower(DiabetesFollower follower) {
			LoadingDialog.hideLoading(FollowerActivity.this);
			if (null != follower) {
				FollowerActivity.this.finish();
			}			
		}		
	};
}
