package coursera.capstone.view.activity;

import java.util.Date;

import coursera.capstone.controller.DiabetesNotificationManager;
import coursera.capstone.model.DiabetesUser;
import coursera.capstone.utils.DiabetesConstants;
import coursera.capstone.view.fragment.DatePickerFragment;
import coursera.capstone.view.fragment.LoadingDialog;

import coursera.capstone.R;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class EditProfileActivity extends UserBaseActivity {
	
	private final static String TAG = EditProfileActivity.class.getSimpleName();
	
	private TextView notificationPeriodTextView;
	private SeekBar notificationPeriodSeekBar;
	private CheckBox ignoreNightCheckBox;
	private TextView notificationNextTextView;
	private long nextNotificationTime;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DiabetesNotificationManager.loadSetting(this);
		nextNotificationTime = DiabetesNotificationManager.getTargetTime(); 
		fillViews();
	}
	
	@Override
	protected void initView() {
		setContentView(R.layout.activity_edit_profile);
	}
	
	private void fillViews() {
		DiabetesUser user = diabetesDataSource.getCurrentUser();
		if (null != user) {
			userTypeSpinner.setSelection(user.getType().ordinal());
			firstNameEditText.setText(user.getFirstName());
			lastNameEditText.setText(user.getLastName());
			birthDayEditText.setText(DatePickerFragment.getDateText(new Date(user.getBirthDay())));
			medicalRecordEditText.setText(user.getMedicalRecord());
			
			notificationPeriodSeekBar.setProgress(periodMsToProgress(user.getNotificationPeriodMs()));
			ignoreNightCheckBox.setChecked(user.isNotificationIgnoreNight());
			updatePeriodText();
			updateNextNotificationText();
		}
	}
	
	@Override
	protected void loadCustomViews() {
		notificationPeriodTextView = (TextView)this.findViewById(R.id.NotificationPeriodTextView);
		notificationPeriodSeekBar = (SeekBar)this.findViewById(R.id.NotificationPeriodSeekBar);
		ignoreNightCheckBox = (CheckBox)this.findViewById(R.id.IgnoreNightCheckBox);
		notificationNextTextView = (TextView)this.findViewById(R.id.NotificationNextTextView);
		
		notificationPeriodSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				updatePeriodText();
				updateNextNotificationText();
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				
			}
		});
		
		ignoreNightCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				updateNextNotificationText();
			}
		});
		
		Button saveButton = (Button)this.findViewById(R.id.SaveButton);
		saveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				save();
			}
		});
	}
	
	private int periodMsToProgress(long periodMs) {
		return (int)(periodMs / DiabetesConstants.HOUR_MS - 1);
	}
	
	private long progressToPeriodMs(int progress) {
		return (progress + 1) * DiabetesConstants.HOUR_MS;
	}
	
	private void updatePeriodText() {
		String text = String.format(getString(R.string.notification_period), notificationPeriodSeekBar.getProgress() + 1);
		notificationPeriodTextView.setText(text);
	}
	
	private void updateNextNotificationText() {
		long newPeriod = progressToPeriodMs(notificationPeriodSeekBar.getProgress());
		boolean newIgnoreNight = ignoreNightCheckBox.isChecked();
		long srcTime = new Date().getTime() - 1;
		long nextCalcTime = DiabetesNotificationManager.calcNextTargetTime(srcTime, newPeriod, false, newIgnoreNight);
		String text = String.format(getString(R.string.notification_next), 
				DiabetesConstants.dateTimeFormater.format(new Date(nextNotificationTime)), 
				DiabetesConstants.dateTimeFormater.format(new Date(nextCalcTime)));
		notificationNextTextView.setText(text);
	}
	
	@Override
	protected DiabetesUser readFields() {
		long newPeriod = progressToPeriodMs(notificationPeriodSeekBar.getProgress());
		boolean newIgnoreNight = ignoreNightCheckBox.isChecked();
		
		DiabetesUser user = super.readFields();
		if (null != user) {
			user.setNotificationPeriodMs(newPeriod);
			user.setNotificationIgnoreNight(newIgnoreNight);
		}		
		return user;
	}
	
	private void save() {
		DiabetesUser user = readFields();
		if (null != user) {
			LoadingDialog.showLoading(this, getString(R.string.registering));
			diabetesDataSource.setProfile(user);
		}
	}
	
    @Override
    protected void onLoadComplete(DiabetesUser diabetesUser) {
    	LoadingDialog.hideLoading(EditProfileActivity.this);
		if (null != diabetesUser) {
			if (diabetesUser.getType() == DiabetesUser.UserType.USER_TYPE_TEEN) {
				DiabetesNotificationManager.setSettings(EditProfileActivity.this,
						diabetesUser.getId(),
						diabetesUser.getNotificationPeriodMs(),
						diabetesUser.isNotificationIgnoreNight());
			} else {
				DiabetesNotificationManager.resetSettings(EditProfileActivity.this);
			}
			Toast.makeText(EditProfileActivity.this, R.string.saved, Toast.LENGTH_SHORT).show();
			EditProfileActivity.this.finish();
		}
    }

}
