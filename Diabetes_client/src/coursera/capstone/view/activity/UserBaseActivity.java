package coursera.capstone.view.activity;

import coursera.capstone.controller.DiabetesDataSource;
import coursera.capstone.model.DiabetesUser;
import coursera.capstone.view.fragment.DatePickerFragment;

import coursera.capstone.R;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public abstract class UserBaseActivity extends Activity {
	
	private final static String TAG = UserBaseActivity.class.getSimpleName();
	
	protected DiabetesDataSource diabetesDataSource;
	
	protected Spinner userTypeSpinner;
	protected EditText firstNameEditText;
	protected EditText lastNameEditText;
	protected ViewGroup teenPropertiesGroup;
	protected EditText birthDayEditText;
	protected EditText medicalRecordEditText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initView();
		diabetesDataSource = DiabetesDataSource.get(this.getApplicationContext());
		ActionBar actionBar = getActionBar();
        if (null != actionBar) {
            actionBar.setDisplayHomeAsUpEnabled(true);            
        }
        loadViews();
	}
	
	@Override
	protected void onPause() {
		diabetesDataSource.setProfileLoadListner(null);
		super.onPause();
	}

	@Override
	protected void onResume() {
		diabetesDataSource.setProfileLoadListner(profileLoadListner);
		super.onResume();
	}

	@Override
	public void onBackPressed() {
	  finish();
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	
	protected abstract void initView();
	
	protected void loadViews() {
		userTypeSpinner = (Spinner)this.findViewById(R.id.UserTypeSpinner);
		firstNameEditText = (EditText)this.findViewById(R.id.FirstNameEditText);
		lastNameEditText = (EditText)this.findViewById(R.id.LastNameEditText);
		teenPropertiesGroup = (ViewGroup)this.findViewById(R.id.TeenPropertiesGroup);
		medicalRecordEditText = (EditText)this.findViewById(R.id.MedicalRecordEditText);
		birthDayEditText = (EditText)this.findViewById(R.id.BirthDayEditText);
		
		userTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				updateType();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
								
			}
		});
		
		birthDayEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    showDatePicker(birthDayEditText);
                }
                return true;
            }
        });
		
		loadCustomViews();
	}
	
	private void updateType() {
		boolean isTeen = (userTypeSpinner.getSelectedItemPosition() == DiabetesUser.UserType.USER_TYPE_TEEN.ordinal());
		teenPropertiesGroup.setVisibility(isTeen ? View.VISIBLE : View.GONE);
	}
	
	protected abstract void loadCustomViews();
	
	protected void showError(int errorTextId) {
		String errorText = getString(errorTextId);
		Log.e(TAG, errorText);
		Toast.makeText(this, errorText, Toast.LENGTH_SHORT).show();
	}
	
    private void showDatePicker(EditText resultView) {
        resultView.requestFocus();
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setEditTextView(resultView);
        datePickerFragment.show(this.getFragmentManager(), "datePicker");
    }
    
    private boolean isTeen(int userType) {
		return userType == DiabetesUser.UserType.USER_TYPE_TEEN.ordinal();
	}
	
	protected DiabetesUser readFields() {
		String first = firstNameEditText.getText().toString();
		String last = lastNameEditText.getText().toString();
		String birthday = birthDayEditText.getText().toString();
		String medical = medicalRecordEditText.getText().toString();
		int userType = userTypeSpinner.getSelectedItemPosition();
		
		if (first.isEmpty()) {
			showError(R.string.error_first_name_empty);
			return null;
		}
		
		if (last.isEmpty()) {
			showError(R.string.error_last_name_empty);
			return null;
		}
		
		if (birthday.isEmpty() && isTeen(userType)) {
			showError(R.string.error_birthday_empty);
			return null;
		}
		
		if (medical.isEmpty() && isTeen(userType)) {
			showError(R.string.error_medical_empty);
			return null;
		}
		
		DiabetesUser user = new DiabetesUser();
		user.setType(userType);
		user.setFirstName(first);
		user.setLastName(last);
		user.setBirthDay(DatePickerFragment.parseDate(birthday).getTime());
		user.setMedicalRecord(medical);
		return user;
	}
    
    protected abstract void onLoadComplete(DiabetesUser diabetesUser);
    
    private DiabetesDataSource.ProfileLoadListner profileLoadListner = new DiabetesDataSource.ProfileLoadListner() {

		@Override
		public void onLoadProfile(DiabetesUser diabetesUser) {
			try {
				onLoadComplete(diabetesUser);
			} catch (Exception e) {
				
			}
		}
    	
    };
}
