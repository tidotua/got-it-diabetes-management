package coursera.capstone.view.activity;

import java.util.Date;

import coursera.capstone.R;
import coursera.capstone.controller.DiabetesDataSource;
import coursera.capstone.controller.DiabetesNotificationManager;
import coursera.capstone.model.CheckInData;
import coursera.capstone.model.DiabetesUser;
import coursera.capstone.utils.DiabetesConstants;
import coursera.capstone.view.fragment.LoadingDialog;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class CheckInActivity  extends Activity  {
	
	private TextView dateTimeTextView;
	private TextView bloodSugarLevelTextView;
	private SeekBar bloodSugarLevelSeekBar;
	private CheckBox administerInsulineCheckBox;
	private ImageView administerInsulineImageView;
	private TextView moodTextView;
	private ImageView moodImageView;
	private SeekBar moodSeekBar;
	private EditText mealEditText;
	private Button checkInButton;
	
	private Date dateTime = new Date();	
	private String[] moodTexts;
	private int normalTextColor;
	private int alarmTextColor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_checkin);
		
		ActionBar actionBar = getActionBar();
        if (null != actionBar) {
            actionBar.setDisplayHomeAsUpEnabled(true);            
        }
        
        moodTexts = getResources().getStringArray(R.array.mood_array);
        normalTextColor = getResources().getColor(R.color.normal_text);
        alarmTextColor = getResources().getColor(R.color.alarm_text);
        loadViews();
        checkLogin();
	}
	
	@Override
	protected void onPause() {
		DiabetesDataSource.get(this).setProfileLoadListner(null);
		super.onPause();
	}

	@Override
	protected void onResume() {
		DiabetesDataSource.get(this).setCheckInDataListner(checkInDataListner);
		super.onResume();
	}
	
	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED);
		finish();
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
            	setResult(RESULT_CANCELED);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	
	private void loadViews() {
		dateTimeTextView = (TextView)findViewById(R.id.DateTimeTextView);
		bloodSugarLevelTextView  = (TextView)findViewById(R.id.BloodSugarLevelTextView);
		bloodSugarLevelSeekBar = (SeekBar)findViewById(R.id.BloodSugarLevelSeekBar);
		administerInsulineCheckBox = (CheckBox)findViewById(R.id.AdministerInsulineCheckBox);
		administerInsulineImageView = (ImageView)findViewById(R.id.AdministerInsulineImageView);
		moodTextView = (TextView)findViewById(R.id.MoodTextView);
		moodImageView = (ImageView)findViewById(R.id.MoodImageView);
		moodSeekBar = (SeekBar)findViewById(R.id.MoodSeekBar);
		mealEditText = (EditText)findViewById(R.id.MealEditText);
		checkInButton = (Button)findViewById(R.id.CheckInButton);
		
		setListners();
		fillViews();
	}
	
	private void setListners() {
		bloodSugarLevelSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				updateBloodSugarLevel();
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				
			}
		});
		
		administerInsulineCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				updateAdministerInsuline();
			}
		});
		
		moodSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				updateMood();
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				
			}
		});
		
		checkInButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				checkIn();
			}
		});
	}
	
	private void fillViews() {
		dateTimeTextView.setText(DiabetesConstants.dateTimeFormater.format(dateTime));
		updateBloodSugarLevel();
		updateAdministerInsuline();
		updateMood();
	}
	
	private void updateBloodSugarLevel() {
		int sugarLevel = bloodSugarLevelSeekBar.getProgress();
		int color = (sugarLevel >= DiabetesConstants.LOW_SUGAR && sugarLevel <= DiabetesConstants.HIGH_SUGAR) ? 
				normalTextColor : alarmTextColor;
		String text = getString(R.string.sugar_normal);
		if (sugarLevel < DiabetesConstants.LOW_SUGAR) {
			text = getString(R.string.sugar_low);
		} else if (sugarLevel > DiabetesConstants.HIGH_SUGAR) {
			text = getString(R.string.sugar_high);
		}
		bloodSugarLevelTextView.setText(String.format("%d (%s)", sugarLevel, text));
		bloodSugarLevelTextView.setTextColor(color);
	}
	
	private void updateAdministerInsuline() {
		administerInsulineImageView.setVisibility(administerInsulineCheckBox.isChecked() ? View.VISIBLE: View.INVISIBLE);
	}
	
	private void updateMood() {
		int moodLevel = getMood();
		int color = (moodLevel >= DiabetesConstants.NORMAL_MOOD) ? normalTextColor : alarmTextColor;
		moodTextView.setText(moodTexts[moodLevel]);
		moodTextView.setTextColor(color);
		moodImageView.setImageResource(DiabetesConstants.MOOD_IMAGES[moodLevel]);
		updateSmileColor(moodImageView, moodLevel);
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	private void updateSmileColor(ImageView smileImageView, int mood) {
		int color = 0;
		if (mood > DiabetesConstants.NORMAL_MOOD) {
			color = getResources().getColor(R.color.mood_good);
		} else if (mood < DiabetesConstants.NORMAL_MOOD) {
			color = getResources().getColor(R.color.mood_bad);
		} else if (DiabetesConstants.NORMAL_MOOD == mood) {
			smileImageView.clearColorFilter();
			return;
		}
        PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN);
        smileImageView.setColorFilter(colorFilter);
	}
	
	private int getMood() {
		double progress = 1.0 * moodSeekBar.getProgress() / moodSeekBar.getMax();
		return (int)((progress + 0.125) * 4);
	}
	
	private void checkIn() {
		DiabetesDataSource diabetesDataSource = DiabetesDataSource.get(this);
		DiabetesUser diabetesUser = diabetesDataSource.getCurrentUser();
		if (null == diabetesUser) {
			Toast.makeText(this, R.string.error_not_logined, Toast.LENGTH_SHORT).show();
			diabetesDataSource.logout();
			startLoginActivity();
			return;
		}
		
		LoadingDialog.showLoading(this, getString(R.string.checking_in));
		CheckInData checkInData = new CheckInData();
		checkInData.setUid(diabetesUser.getId());
		checkInData.setDateTime(dateTime.getTime());
		checkInData.setBloodSugarLevel((short)bloodSugarLevelSeekBar.getProgress());
		checkInData.setMood((short)getMood());
		checkInData.setAdministerInsulin(administerInsulineCheckBox.isChecked());
		checkInData.setMealText(mealEditText.getText().toString());
		
		diabetesDataSource.checkIn(checkInData);
	}
	
	private void checkLogin() {
		if (!DiabetesDataSource.get(CheckInActivity.this).isLogined()) {
			startLoginActivity();
		}
	}
	
	private void startLoginActivity() {
		startActivity(new Intent(this, LoginActivity.class));
	}
	
	DiabetesDataSource.CheckInDataListner checkInDataListner = new DiabetesDataSource.CheckInDataListner() {
		@Override
		public void onCheckIn(CheckInData checkInData) {
			try {
				LoadingDialog.hideLoading(CheckInActivity.this);
				if (null != checkInData) {
					int causeTextId = DiabetesNotificationManager.getCauseTextId(checkInData);
					DiabetesNotificationManager.checkInAlarm(CheckInActivity.this, causeTextId);
					setResult(RESULT_OK);
					finish();
				} else {
					checkLogin();
				}
			} catch (Exception e) {
				
			}
		}		
	};

}
