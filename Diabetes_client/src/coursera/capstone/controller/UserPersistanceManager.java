package coursera.capstone.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class UserPersistanceManager {

	private final static String USER_PREFERENCES = "ldata";
	private final static String USER_LOGIN = "coursera.capstone.user.login";
	private final static String USER_PASSWORD = "coursera.capstone.user.pass";
	
	public class Credentials {
		public String login;
		public String password;
		
		public Credentials(String login, String password) {
			this.login = login;
			this.password = password;
		}
	};
	
	private SharedPreferences getPreferences(Context context) {
		return context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE);
	}
	
	public void save(Context context, String login, String password) {
		SharedPreferences prefs = getPreferences(context);
		Editor editor = prefs.edit();
		editor.putString(USER_LOGIN, login);
		editor.putString(USER_PASSWORD, password);
		editor.commit();
	}
	
	public void clear(Context context) {
		SharedPreferences prefs = getPreferences(context);
		Editor editor = prefs.edit();
		editor.remove(USER_LOGIN);
		editor.remove(USER_PASSWORD);
		editor.commit();
	}
	
	public Credentials load(Context context) {
		SharedPreferences prefs = getPreferences(context);
		String login = prefs.getString(USER_LOGIN, null);
		String password = prefs.getString(USER_PASSWORD, null);
		return (null == login || null == password) ? null : new Credentials(login, password);
	}

}
