package coursera.capstone.controller;

import java.util.Calendar;
import java.util.Date;

import coursera.capstone.R;
import coursera.capstone.model.CheckInData;
import coursera.capstone.utils.DiabetesConstants;
import coursera.capstone.view.activity.MainActivity;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


public class DiabetesNotificationManager
		extends BroadcastReceiver {
	
	private final static String TAG = DiabetesNotificationManager.class.getSimpleName();
	
	private final static int DEFAULT_UID = 0; // 0 for follower/reset
	
	private final static int NIGHT_START_HOUR = 23;
	private final static int NIGHT_END_HOUR = 7;
	private final static int NIGHT_LEN_HOUR = (24 + NIGHT_END_HOUR - NIGHT_START_HOUR) % 24;
	
	private final static String NOTIFICATION_SETTING_UID = "coursera.capstone.setting.uid";
	private final static String NOTIFICATION_SETTING_PERIOD = "coursera.capstone.setting.period";
	private final static String NOTIFICATION_SETTING_IGNORE_NIGHT = "coursera.capstone.setting.ignore_night";
	private final static String NOTIFICATION_SETTING_TARGET_TIME = "coursera.capstone.setting.target_time";
	private final static String NOTIFICATION_SETTING_CAUSE_TEXT_ID = "coursera.capstone.setting.cause_text_id";
	
	private final static String NOTIFICATION_PARAMETER_TITLE = "title";
	private final static String NOTIFICATION_PARAMETER_TEXT = "text";
	private final static String NOTIFICATION_PARAMETER_ID = "id";
	
	private static long notificationUid;
	private static long period;
	private static boolean ignoreNight;
	private static long targetTime;
	private static int causeStringId;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			Bundle bundle = intent.getExtras();
		    makeNotification(
		    		context,
		    		bundle.getString(NOTIFICATION_PARAMETER_TITLE),
		    		bundle.getString(NOTIFICATION_PARAMETER_TEXT),
		    		bundle.getInt(NOTIFICATION_PARAMETER_ID));
		    updateAlarm(context);
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }	
	}

	
	public static void checkInAlarm(Context context, int newCauseTextId) {
		loadSetting(context);
		causeStringId = newCauseTextId;
		targetTime = new Date().getTime() - 1;
		calcNextTargetTime(false);
		saveSetting(context);
		setAlarm(context, targetTime, causeStringId);
	}
	
	public static void updateAlarm(Context context)
	{
		loadSetting(context);
		calcNextTargetTime(true);
		saveSetting(context);
		setAlarm(context, targetTime, causeStringId);
	}
	
	private static void setAlarm(Context context, long timeMs, int textId) {
		AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, DiabetesNotificationManager.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 
				PendingIntent.FLAG_UPDATE_CURRENT);
        alarmMgr.cancel(pendingIntent);
        
        if (!isNotificationEnabled()) {
        	Log.i(TAG, "Cancel Alarm");
        	return;
        }
		
    	intent.putExtra(NOTIFICATION_PARAMETER_TITLE, context.getString(R.string.notification_title));
		intent.putExtra(NOTIFICATION_PARAMETER_TEXT, context.getString(textId));
		intent.putExtra(NOTIFICATION_PARAMETER_ID, 0);
		pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 
				PendingIntent.FLAG_UPDATE_CURRENT);
        alarmMgr.set(AlarmManager.RTC_WAKEUP, timeMs, pendingIntent);
        Log.i(TAG, "Set Alarm " + DiabetesConstants.dateTimeFormater.format(new Date(timeMs)));
	}
	
	private static void makeNotification(Context context, String caption, String text, int nid)
	{
		Intent intent = new Intent(context, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.putExtra(MainActivity.FLAG_START_CHECKIN, true);
		PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		Notification notification = new NotificationCompat.Builder(context)
			.setContentTitle(caption)
		    .setContentText(text)
		    .setSmallIcon(R.drawable.ic_notification)
		    .setContentIntent(pIntent)
		    .setAutoCancel(true)
		    .build();
		notification.defaults |= Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
		
		NotificationManager notificationManager = 
		  (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(nid, notification);
		Log.i(TAG, "Notify: " + text);
	}
	
	public static long getTargetTime() {
		return targetTime;
	}
	
	private static boolean isNotificationEnabled() {
		return notificationUid != DEFAULT_UID;
	}
	
	public static int getCauseTextId(CheckInData checkInData) {
		if (checkInData.getBloodSugarLevel() < DiabetesConstants.LOW_SUGAR || checkInData.getBloodSugarLevel() > DiabetesConstants.HIGH_SUGAR) {
			return R.string.notification_bad_sugar_text;
		}
		if (!checkInData.isAdministerInsulin()) {
			return R.string.notification_insuline_text;
		}
		if (checkInData.getMood() < DiabetesConstants.NORMAL_MOOD) {
			return R.string.notification_bad_mood_text;
		}
		return R.string.notification_default_text;
	}
	
	public static void setSettings(Context context, long uid, long newPeriod, boolean newIgnoreNight) {
		loadSetting(context);
		boolean needTimeUpdate = (uid != notificationUid || period != newPeriod || ignoreNight != newIgnoreNight);
		boolean needCauseUpdate = (uid != notificationUid);
		if (!needTimeUpdate) {
			return;
		}
		notificationUid = uid;
		period = newPeriod;
		ignoreNight = newIgnoreNight;
		targetTime = new Date().getTime() - 1;
		if (needCauseUpdate) {
			causeStringId = R.string.notification_default_text;
		}		
		calcNextTargetTime(false);
		saveSetting(context);
		setAlarm(context, targetTime, causeStringId);
	}
	
	public static void resetSettings(Context context) {
		loadSetting(context);
		notificationUid = DEFAULT_UID;
		period = DiabetesConstants.MAX_CHECKIN_TIMEOUT;
		ignoreNight = true;
		targetTime = new Date().getTime();
		causeStringId = R.string.notification_default_text;
		saveSetting(context);
		setAlarm(context, targetTime, causeStringId);
	}
	
	public static void saveSetting(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = prefs.edit();
		editor.putLong(NOTIFICATION_SETTING_UID, notificationUid);
		editor.putLong(NOTIFICATION_SETTING_PERIOD, period);
		editor.putBoolean(NOTIFICATION_SETTING_IGNORE_NIGHT, ignoreNight);
		editor.putLong(NOTIFICATION_SETTING_TARGET_TIME, targetTime);
		editor.putInt(NOTIFICATION_SETTING_CAUSE_TEXT_ID, causeStringId);
		editor.commit();
	}
	
	public static void loadSetting(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		notificationUid = prefs.getLong(NOTIFICATION_SETTING_UID, DEFAULT_UID);
		period = prefs.getLong(NOTIFICATION_SETTING_PERIOD, DiabetesConstants.MAX_CHECKIN_TIMEOUT);
		ignoreNight = prefs.getBoolean(NOTIFICATION_SETTING_IGNORE_NIGHT, true);
		targetTime = prefs.getLong(NOTIFICATION_SETTING_TARGET_TIME, new Date().getTime());
		causeStringId = prefs.getInt(NOTIFICATION_SETTING_CAUSE_TEXT_ID, R.string.notification_default_text);
	}
	
	public static long calcNextTargetTime(long tTime, long nPeriod, boolean notifyLate, boolean ignNight) {
		long now = new Date().getTime();
		if (now < tTime) {
			return tTime;
		}
		if (notifyLate) {
			tTime = now + DiabetesConstants.LATE_NOTIFICATION_MINUTES;
			Log.i(TAG, "Late Notification"); // DEBUG
			return tTime;
		}
		
		tTime = now + nPeriod;
		if (ignNight && inThisNight(tTime)) {
			long targetNew = now + nPeriod / 2;
			if (!inThisNight(targetNew)) {
				tTime = getThisNightStart();
			} else {
				tTime = getThisNightEnd();
			}
		}
		return tTime;
	}
	
	private static long calcNextTargetTime(boolean notifyLate) { // all setting loaded
		targetTime = calcNextTargetTime(targetTime, period, notifyLate, ignoreNight);
		return targetTime;
	}
	
	private static long getThisNightStart() {
		return getDateHour(new Date().getTime() - NIGHT_END_HOUR * DiabetesConstants.HOUR_MS, NIGHT_START_HOUR);
	}
	
	private static long getThisNightEnd() {
		return getThisNightStart() + NIGHT_LEN_HOUR * DiabetesConstants.HOUR_MS;
	}
	
	private static boolean inThisNight(long timeMs) {
		return (timeMs > getThisNightStart() && timeMs < getThisNightEnd());
	}
	
	public static long getDateHour(long timeMs, int hour) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(timeMs));
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime().getTime();
	}
}

