package coursera.capstone.controller;

import java.util.Collection;

import coursera.capstone.model.CheckInData;
import coursera.capstone.model.DiabetesFollower;
import coursera.capstone.model.DiabetesUser;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface DiabetesRestService {
	
	public static final String AUTH_PATH = "/oauth/token";
	
	//---------------------------------------------------------------------------------------------
	
	public static final String REGISTER_PATH = "/register";
	public static final String USERS_PATH = "/users";
	public static final String PROFILE_PATH = USERS_PATH + "/profile";
	
	@POST(REGISTER_PATH)
	public DiabetesUser register(@Body DiabetesUser diabetesUser);
	
	@GET(PROFILE_PATH)
	public DiabetesUser getProfile();
	
	@POST(PROFILE_PATH)
	public DiabetesUser setProfile(@Body DiabetesUser diabetesUser);
	
	//---------------------------------------------------------------------------------------------
	
	public static final String FEEDBACK_SVC_PATH = "/feedback";
	public static final String WATCHLIST_FEEDBACK_PATH = "/feedback/watchlist/{wuid}";
	
	@GET(FEEDBACK_SVC_PATH)
	public Collection<CheckInData> getFeedback();
	
	@GET(WATCHLIST_FEEDBACK_PATH)
	public Collection<CheckInData> getWatchlistFeedback(@Path("wuid") long wuid);
	
	@POST(FEEDBACK_SVC_PATH)
	public CheckInData checkIn(@Body CheckInData checkInData);
	
	//---------------------------------------------------------------------------------------------
	
	public static final String FOLLOWER_PATH = USERS_PATH + "/follower";
	public static final String WATCHLIST_PATH = USERS_PATH + "/watchlist";
	public static final String FOLLOWER_REMOVE_PATH = USERS_PATH + FOLLOWER_PATH + "/remove";
	
	@GET(FOLLOWER_PATH)
	public Collection<DiabetesFollower> getFollowers();
	
	@GET(WATCHLIST_PATH)
	public Collection<DiabetesFollower> getWatchlist();
	
	@POST(FOLLOWER_PATH + "/add/{username}")
	public DiabetesFollower addFollower(@Path("username") String username);
	
	@POST(WATCHLIST_PATH + "/add/{username}")
	public DiabetesFollower addToWatchlist(@Path("username") String username);
	
	@POST(FOLLOWER_PATH)
	public DiabetesFollower setFollower(@Body DiabetesFollower follower);
	
	@POST(FOLLOWER_REMOVE_PATH)
	public Void removeFollower(@Body DiabetesFollower follower);
	
}
