package coursera.capstone.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class RebootReceiver
extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		DiabetesNotificationManager.updateAlarm(context);
	}

}
