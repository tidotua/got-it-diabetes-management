package coursera.capstone.controller;

import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Date;

import coursera.capstone.R;
import coursera.capstone.controller.secured.SecuredRestBuilder;
import coursera.capstone.controller.secured.SecuredRestException;
import coursera.capstone.controller.secured.UnsafeHttpsClient;
import coursera.capstone.model.CheckInData;
import coursera.capstone.model.DiabetesFollower;
import coursera.capstone.model.DiabetesUser;
import coursera.capstone.utils.DiabetesConstants;
import coursera.capstone.view.fragment.LoadingDialog;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.RetrofitError;
import retrofit.client.OkClient;

public class DiabetesDataSource {
	
	private final static String TAG = DiabetesDataSource.class.getSimpleName();
	
	public interface ProfileLoadListner {
		public void onLoadProfile(DiabetesUser diabetesUser);
	};
	
	public interface FeedbackListner {
		public void onLoadFeedbacks(Collection<CheckInData> feedbacks);
	};
	
	public interface CheckInDataListner {
		public void onCheckIn(CheckInData checkInData);
	};
	
	public interface FollowerListner {
		public void onLoadFollower(DiabetesFollower follower);
	};
	
	public interface FollowersListner {
		public void onLoadFollowers(Collection<DiabetesFollower> followers);
	};

	private static DiabetesDataSource diabetesDataSource;
	
	public static DiabetesDataSource get(Context context) {
		if (null == diabetesDataSource) {
			diabetesDataSource = new DiabetesDataSource(context);
		}
		return diabetesDataSource;
	}
	
	private Context context;
	private Handler handler;
	private volatile DiabetesRestService diabetesRestService;
	private volatile long lastDataChengeDt;
	private volatile WeakReference<ProfileLoadListner> profileLoadListner;
	private volatile WeakReference<FeedbackListner> feedbackListner;
	private volatile WeakReference<CheckInDataListner> checkInDataListner;
	private volatile WeakReference<FollowerListner> followerListner;
	private volatile WeakReference<FollowersListner> followersListner;
	private volatile DiabetesUser currentUser;
	
	
	private DiabetesDataSource(Context context) {
		this.context = context;
		handler = new Handler();
		lastDataChengeDt = new Date().getTime();
	}
	
	public void setProfileLoadListner(ProfileLoadListner profileLoadListner) {
		this.profileLoadListner = new WeakReference<ProfileLoadListner>(profileLoadListner);
	}
	
	public void setFeedbackListner(FeedbackListner feedbackListner) {
		this.feedbackListner = new WeakReference<FeedbackListner>(feedbackListner);
	}
	
	public void setCheckInDataListner(CheckInDataListner checkInDataListner) {
		this.checkInDataListner = new WeakReference<CheckInDataListner>(checkInDataListner);
	}
	
	public void setFollowerListner(FollowerListner followerListner) {
		this.followerListner = new WeakReference<FollowerListner>(followerListner);
	}
	
	public void setFollowersListner(FollowersListner followersListner) {
		this.followersListner = new WeakReference<FollowersListner>(followersListner);
	}	
	
	public long getLastDataChangeDt() {
		return lastDataChengeDt;
	}
	
	public boolean isLogined() {
		return null != diabetesRestService;
	}
	
	public String getUserName() {
		if (null != currentUser) {
			return currentUser.getUsername();
		}
		return "";
	}
	
	public DiabetesUser.UserType getUserType() {
		if (null != currentUser) {
			return currentUser.getType();
		}
		return DiabetesUser.UserType.USER_TYPE_TEEN;
	}
	
	public DiabetesUser getCurrentUser() {
		return currentUser;
	}
	
	public void login(String username, String password) {
		Log.i(TAG, "Login");
		RestAdapter restAdapter = new SecuredRestBuilder()
			.setLoginEndpoint(DiabetesConstants.SERVICE_BASE_URL + DiabetesRestService.AUTH_PATH)
			.setUsername(username)
			.setPassword(password)
			.setClientId("mobile")
			.setClient(new OkClient(UnsafeHttpsClient.getUnsafeOkHttpClient()))
			.setEndpoint(DiabetesConstants.SERVICE_BASE_URL)
			.setLogLevel(LogLevel.FULL) 
			.build();
		diabetesRestService = restAdapter.create(DiabetesRestService.class);
		lastDataChengeDt = new Date().getTime();
		getProfile();
	}
	
	public void logout() {
		diabetesRestService = null;
		lastDataChengeDt = new Date().getTime();
		currentUser = null;
		Log.i(TAG, "Logout");
	}
	
	public void register(final DiabetesUser diabetesUser) {
		Log.i(TAG, "Register");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					RestAdapter registerRestAdapter = new RestAdapter.Builder()
			    		.setEndpoint(DiabetesConstants.SERVICE_BASE_URL)
			    		.setClient(new OkClient(UnsafeHttpsClient.getUnsafeOkHttpClient()))
			    		.build();
					DiabetesRestService registerRestService = registerRestAdapter.create(DiabetesRestService.class);
					DiabetesUser newDiabetesUser = registerRestService.register(diabetesUser);
					Log.i(TAG, "Profile registered");
					postProfile(newDiabetesUser);
				} catch(Exception e) {
					Log.e(TAG, "Registere error: " + e.getMessage());
					postError(R.string.error_not_registered_other);
					postProfile(null);
				}
			}
		}).start();
	}
	
	private void getProfile() {
		Log.i(TAG, "Get user profile");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					postProfile(diabetesRestService.getProfile());
				} catch (Exception e) {
					if (!fixSecurityError(e)) {
						Log.e(TAG, "Profile fetch error: " + e.getMessage());
						postError(R.string.error_profile_load_failed);
					}
					postProfile(null);
				}
			}
		}).start();
	}
	
	public void setProfile(final DiabetesUser diabetesUser) {
		Log.i(TAG, "Set user profile");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (!isLogined()) {
						postError(R.string.error_not_logined);
						postProfile(null);
						return;
					}
					postProfile(diabetesRestService.setProfile(diabetesUser));
				} catch (Exception e) {
					if (!fixSecurityError(e)) {
						Log.e(TAG, "Profile save error: " + e.getMessage());
						postError(R.string.error_profile_save_failed);
					}
					postProfile(null);
				}
			}
		}).start();
	}
	
	public void checkIn(final CheckInData checkInData) {
		Log.i(TAG, "Check In");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (!isLogined()) {
						postError(R.string.error_not_logined);
						postCheckIn(null);
						return;
					}
					CheckInData resultCheckInData = diabetesRestService.checkIn(checkInData); 
					lastDataChengeDt = new Date().getTime();
					postCheckIn(resultCheckInData);
				} catch (Exception e) {
					if (!fixSecurityError(e)) {
						Log.e(TAG, "Check In error: " + e.getMessage());
						postError(R.string.error_checkin_failed);
					}
					postCheckIn(null);
				}
			}
		}).start();
	}
	
	public void getFeedback() {
		Log.i(TAG, "Get feedback");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (!isLogined()) {
						postError(R.string.error_not_logined);
						postFeedback(null);
						return;
					}
					Collection<CheckInData> feedback = diabetesRestService.getFeedback(); 
					lastDataChengeDt = new Date().getTime();
					postFeedback(feedback);
				} catch (Exception e) {
					if (!fixSecurityError(e)) {
						Log.e(TAG, "Feedback fetch error: " + e.getMessage());
						postError(R.string.error_feedback_failed);
					}
					postFeedback(null);
				}
			}
		}).start();
	}
	
	public void getWatchlistFeedback(final long wuid) {
		Log.i(TAG, "Get watchlist feedback");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (!isLogined()) {
						postError(R.string.error_not_logined);
						postFeedback(null);
						return;
					}
					Collection<CheckInData> feedback = diabetesRestService.getWatchlistFeedback(wuid); 
					postFeedback(feedback);
				} catch (Exception e) {
					if (!fixSecurityError(e)) {
						Log.e(TAG, "Watchlist feedback fetch error: " + e.getMessage());
						postError(R.string.error_feedback_failed);
					}
					postFeedback(null);
				}
			}
		}).start();
	}
	
	public void addFollower(final String username) {
		Log.i(TAG, "Add follower");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (!isLogined()) {
						postError(R.string.error_not_logined);
						postFollower(null);
						return;
					}
					DiabetesFollower follower = diabetesRestService.addFollower(username);
					postFollower(follower);
				} catch (Exception e) {
					if (!fixSecurityError(e)) {
						Log.e(TAG, "Follower add error: " + e.getMessage());
						postError(R.string.error_follower_add);
					}
					postFollower(null);
				}
			}
		}).start();
	}
	
	public void addToWatchlist(final String username) {
		Log.i(TAG, "Add to watchlist");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (!isLogined()) {
						postError(R.string.error_not_logined);
						postFollower(null);
						return;
					}
					DiabetesFollower follower = diabetesRestService.addToWatchlist(username);
					postFollower(follower);
				} catch (Exception e) {
					if (!fixSecurityError(e)) {
						Log.e(TAG, "Watchlist add error: " + e.getMessage());
						postError(R.string.error_watchlist_add);
					}
					postFollower(null);
				}
			}
		}).start();
	}
	
	public void getFollowers() {
		Log.i(TAG, "Get followers");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (!isLogined()) {
						postError(R.string.error_not_logined);
						postFollowers(null);
						return;
					}
					Collection<DiabetesFollower> followers = diabetesRestService.getFollowers(); 
					postFollowers(followers);
				} catch (Exception e) {
					if (!fixSecurityError(e)) {
						Log.e(TAG, "Followers fetch error: " + e.getMessage());
						postError(R.string.error_followers_load);
					}
					postFollowers(null);
				}
			}
		}).start();
	}
	
	public void getWatchlist() {
		Log.i(TAG, "Get watchlist");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (!isLogined()) {
						postError(R.string.error_not_logined);
						postFollowers(null);
						return;
					}
					Collection<DiabetesFollower> followers = diabetesRestService.getWatchlist(); 
					postFollowers(followers);
				} catch (Exception e) {
					if (!fixSecurityError(e)) {
						Log.e(TAG, "Watchlist fetch error: " + e.getMessage());
						postError(R.string.error_watchlist_load);
					}
					postFollowers(null);
				}
			}
		}).start();
	}
	
	public void removeFollower(final DiabetesFollower follower) {
		Log.i(TAG, "Remove follower");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (!isLogined()) {
						postError(R.string.error_not_logined);
						postFollower(null);
						return;
					}
					diabetesRestService.removeFollower(follower);
					postFollower(follower); // just not null, don't use it!
				} catch (Exception e) {
					if (!fixSecurityError(e)) {
						Log.e(TAG, "Follower remove error: " + e.getMessage());
						postError(R.string.error_follower_remove);
					}
					postFollower(null);
				}
			}
		}).start();
	}
	
	public void setFollower(final DiabetesFollower follower) {
		Log.i(TAG, "Set follower");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (!isLogined()) {
						postError(R.string.error_not_logined);
						postFollower(null);
						return;
					}
					postFollower(diabetesRestService.setFollower(follower));
				} catch (Exception e) {
					if (!fixSecurityError(e)) {
						Log.e(TAG, "Follower set error: " + e.getMessage());
						postError(R.string.error_follower_save);
					}
					postFollower(null);
				}
			}
		}).start();
	}
	
	private boolean isSecurityError(Exception e) {
		
		if (e.getClass().equals(RetrofitError.class) && null != ((RetrofitError)e).getResponse()) {
			if (((RetrofitError)e).getResponse().getStatus() == 401) {
				return true;
			}
		}
		
		return e.getClass().equals(RetrofitError.class) &&
				null != e.getCause() &&
				e.getCause().getClass().equals(SecuredRestException.class);
	}
	
	private boolean fixSecurityError(Exception e) {
		if (isSecurityError(e)) {
			postError(R.string.error_security);
			Log.e(TAG, "Security error: " + e.getMessage());
			logout();
			return true;
		}
		return false;
	}
	
	private void postProfile(final DiabetesUser diabetesUser) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				if (null != profileLoadListner.get()) {
					currentUser = diabetesUser;
					profileLoadListner.get().onLoadProfile(diabetesUser);
				} else {
					LoadingDialog.hideLoading();
				}
			}
		});
	}
	
	private void postCheckIn(final CheckInData checkInData) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				if (null != checkInDataListner.get()) {
					checkInDataListner.get().onCheckIn(checkInData);
				} else {
					LoadingDialog.hideLoading();
				}
			}					
		});
	}
	
	private void postFeedback(final Collection<CheckInData> feedbackData) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				if (null != feedbackListner.get()) {
					feedbackListner.get().onLoadFeedbacks(feedbackData);
				} else {
					LoadingDialog.hideLoading();
				}
			}
		});
	}
	
	private void postFollower(final DiabetesFollower follower) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				if (null != followerListner.get()) {
					followerListner.get().onLoadFollower(follower);
				} else {
					LoadingDialog.hideLoading();
				}
			}					
		});
	}
	
	private void postFollowers(final Collection<DiabetesFollower> followers) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				if (null != followersListner.get()) {
					followersListner.get().onLoadFollowers(followers);
				} else {
					LoadingDialog.hideLoading();
				}
			}					
		});
	}
	
	private void postError(final int toastStringId) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(context, toastStringId, Toast.LENGTH_SHORT).show();
			}					
		});
	}
	
}
