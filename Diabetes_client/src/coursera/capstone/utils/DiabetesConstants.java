package coursera.capstone.utils;

import java.text.SimpleDateFormat;
import java.util.Locale;

import coursera.capstone.R;

final public class DiabetesConstants {
	
	public static final String SERVICE_BASE_URL = "https://192.168.10.104:8443";
	
	public static final String EXTRA_VIDEO_ID = "vandy.mooc.EXTRA_VIDEO_ID";
	
	public static final int MAX_FILE_SIZE = 50 * 1024 * 1024;
	public static final boolean USE_SERVICES = false;
	
	public static final int VIDEO_SERVICE_MESSAGE = 12345;
	public static final int NOTIFICATION_ID = 1003;
	
	public final static String EXTRA_MESSENGER = "messenger";
	public final static String EXTRA_ID = "id";
	public final static String EXTRA_TITLE = "title";
	public final static String EXTRA_RESULT = "result";
	
	public final static int[] MOOD_IMAGES = {
			R.drawable.mood_worse, 
			R.drawable.mood_bad, 
			R.drawable.mood_normal, 
			R.drawable.mood_good, 
			R.drawable.mood_best
		};
	
	public final static int BAD_MOOD = 1;
	public final static int NORMAL_MOOD = 2;
	public final static int GOOD_MOOD = 3;
	
	public final static int LOW_SUGAR = 70;
	public final static int HIGH_SUGAR = 150;
	public final static int MAX_SUGAR = 250;
	
	public final static long LIST_CACHE_TIMEOUT = 30 * 1000;
	
	public final static long MINUTE_MS = 60 * 1000;
	public final static long HOUR_MS = 60 * 60 * 1000;
	public final static long MIN_CHECKIN_TIMEOUT = 1 * HOUR_MS;
	public final static long MAX_CHECKIN_TIMEOUT = 8 * HOUR_MS;
	public final static long LATE_NOTIFICATION_MINUTES = 30 * MINUTE_MS;
	
	public final static SimpleDateFormat dateTimeFormater = new SimpleDateFormat("MM/dd/yy hh:mm a", Locale.US);
	public final static SimpleDateFormat dateFormater = new SimpleDateFormat("MM/dd/yy", Locale.US);
	public final static SimpleDateFormat timeFormater = new SimpleDateFormat("hh:mm a", Locale.US);
	
}
