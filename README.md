# android-capstone

These are a source code and documentation of my Android Capstone Project 2015 (Got It Diabetes Management).
https://www.coursera.org/course/androidcapstone

The Diabetes Management solution is based on client-server architecture so it consists of server and client.
Server is a RESTful backend based on Spring Java Framework.
Client is an Android mobile application based on author’s Android Security MOOC mini-project. 

The Android client screencast:

https://vimeo.com/144508905

____________________________________________

In server's VM Arguments, provide the following information to use the default keystore provided with the sample code:

   -Dkeystore.file=src/main/resources/private/keystore -Dkeystore.pass=changeit 

In client code please specify  URL address of server
Diabetes_client/src/coursera/capstone/utils/DiabetesConstants.java (line 10):

public static final String SERVICE_BASE_URL = "https://192.168.10.104:8443";    //  put your (local) address of server here
