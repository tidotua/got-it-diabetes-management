package coursera.capstone.diabetes.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.Collection;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import coursera.capstone.diabetes.model.CheckInData;
import coursera.capstone.diabetes.model.DiabetesFollower;
import coursera.capstone.diabetes.model.DiabetesUser;
import coursera.capstone.diabetes.repository.CheckInDataRepository;
import coursera.capstone.diabetes.repository.DiabetesFollowerRepository;
import coursera.capstone.diabetes.repository.DiabetesUserRepository;

@Controller
public class DiabetesController {

	// --------------------------------------------------------------------------------------------
	// USER REQUESTS
	// --------------------------------------------------------------------------------------------
	
	public static final String REGISTER_PATH = "/register";
	public static final String USERS_PATH = "/users";
	public static final String PROFILE_PATH = USERS_PATH + "/profile";
	
	@Autowired
	private DiabetesUserRepository diabetesUserRepository;
		
	@RequestMapping(value = REGISTER_PATH, method = RequestMethod.POST)
	public @ResponseBody DiabetesUser register(@RequestBody DiabetesUser diabetesUser,
			HttpServletResponse response) throws IOException {
		if (null != diabetesUserRepository.findByUsername(diabetesUser.getUsername().toLowerCase())) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
		diabetesUser.setAuthorities("USER");
		diabetesUserRepository.save(diabetesUser);
		return diabetesUser;
	}
	
	@RequestMapping(value = PROFILE_PATH, method = RequestMethod.GET)
	public @ResponseBody DiabetesUser getProfile(Principal principal, HttpServletResponse response) throws IOException {
		DiabetesUser diabetesUser = diabetesUserRepository.findByUsername(principal.getName());
		if (null == diabetesUser) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		return diabetesUser;
	}
	
	@RequestMapping(value = PROFILE_PATH, method = RequestMethod.POST)
	public @ResponseBody DiabetesUser setProfile(@RequestBody DiabetesUser sourceDiabetesUser, 
			Principal principal, HttpServletResponse response) throws IOException {
		DiabetesUser diabetesUser = diabetesUserRepository.findByUsername(principal.getName());
		if (null == diabetesUser) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		
		diabetesUser.setFields(sourceDiabetesUser);
		diabetesUserRepository.save(diabetesUser);
		return diabetesUser;
	}
	
	// --------------------------------------------------------------------------------------------
	// CHECKIN DATA REQUESTS
	// --------------------------------------------------------------------------------------------
	
	public static final String FEEDBACK_PATH = "/feedback";
	public static final String WATCHLIST_FEEDBACK_PATH = "/feedback/watchlist/{wuid}";
	
	@Autowired
	private CheckInDataRepository checkInDataRepository;
	
	@RequestMapping(value = FEEDBACK_PATH, method = RequestMethod.GET)
	public @ResponseBody Collection<CheckInData> getFeedback(Principal principal,
			HttpServletResponse response) throws IOException {
		DiabetesUser diabetesUser = diabetesUserRepository.findByUsername(principal.getName());
		if (null == diabetesUser) {
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		if (!diabetesUser.isTeen()) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
		
		return checkInDataRepository.findByUidOrderByDateTimeDesc(diabetesUser.getId(), new PageRequest(0, 100));
	}
	
	@RequestMapping(value = WATCHLIST_FEEDBACK_PATH, method = RequestMethod.GET)
	public @ResponseBody Collection<CheckInData> getWatchlistFeedback(
			@PathVariable("wuid") long wuid,
			Principal principal,
			HttpServletResponse response) throws IOException {
		
		DiabetesUser followerUser = diabetesUserRepository.findByUsername(principal.getName());
		DiabetesUser wUser = diabetesUserRepository.findOne(wuid);
		
		if (null == followerUser || null == wUser) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		
		DiabetesFollower repoFollower = 
				followerRepository.findByUserAndFollower(wUser, followerUser);
		
		if (null == repoFollower || !repoFollower.isApproved() || !wUser.isTeen()) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
		
		Collection<CheckInData> feedback = checkInDataRepository.findByUidOrderByDateTimeDesc(wuid, new PageRequest(0, 100));
		if (null == feedback) {
			return feedback;
		}
		
		return repoFollower.filterFeedbackData(feedback);
	}
	
	
	@RequestMapping(value = FEEDBACK_PATH, method = RequestMethod.POST)
	public @ResponseBody CheckInData checkIn(@RequestBody CheckInData checkInData,
			Principal principal, HttpServletResponse response) throws IOException {
		
		DiabetesUser diabetesUser = diabetesUserRepository.findByUsername(principal.getName());
		if (null == diabetesUser || checkInData.getUid() != diabetesUser.getId()) {
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		
		if (!diabetesUser.isTeen()) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
		
		return checkInDataRepository.save(checkInData);
	}
	
	// --------------------------------------------------------------------------------------------
	// FOLLOWER REQUESTS
	// --------------------------------------------------------------------------------------------
	
	public static final String FOLLOWER_PATH = USERS_PATH + "/follower";
	public static final String WATCHLIST_PATH = USERS_PATH + "/watchlist";
	public static final String FOLLOWER_REMOVE_PATH = USERS_PATH + FOLLOWER_PATH + "/remove";
	
	@Autowired
	private DiabetesFollowerRepository followerRepository;
	
	@RequestMapping(value = FOLLOWER_PATH, method = RequestMethod.GET)
	public @ResponseBody Collection<DiabetesFollower> getFollowers(Principal principal,
			HttpServletResponse response) throws IOException {
		DiabetesUser diabetesUser = diabetesUserRepository.findByUsername(principal.getName());
		if (null == diabetesUser || !diabetesUser.isTeen()) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
		
		Collection<DiabetesFollower> followers = 
				followerRepository.findByUserOrderByFollowerFirstNameAscFollowerLastNameAsc(diabetesUser); 
		for (DiabetesFollower f: followers) {
			f.setFullName(f.getFollower().getFullName());
		}
		return followers;
	}
	
	@RequestMapping(value = WATCHLIST_PATH, method = RequestMethod.GET)
	public @ResponseBody Collection<DiabetesFollower> getWatchlist(Principal principal,
			HttpServletResponse response) throws IOException {
		DiabetesUser diabetesUser = diabetesUserRepository.findByUsername(principal.getName());
		if (null == diabetesUser) {
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		
		Collection<DiabetesFollower> followers = 
				followerRepository.findByFollowerAndUserTypeOrderByUserFirstNameAscUserLastNameAsc(diabetesUser, DiabetesUser.TEEN_TYPE); 
		for (DiabetesFollower f: followers) {
			f.setFullName(f.getUser().getFullName());
		}
		return followers;
	}
	
	@RequestMapping(value = FOLLOWER_PATH + "/add/{username}", method = RequestMethod.POST)
	public @ResponseBody DiabetesFollower addFollower(
			@PathVariable("username") String username,
			Principal principal,
			HttpServletResponse response) throws IOException {
		
		
		DiabetesUser followerUser = diabetesUserRepository.findByUsername(username.toLowerCase()); 
		DiabetesUser diabetesUser = diabetesUserRepository.findByUsername(principal.getName());
		
		if (null == followerUser || null == diabetesUser) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		
		if (diabetesUser.getId() == followerUser.getId() || !diabetesUser.isTeen()) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
		
		DiabetesFollower repoFollower = followerRepository.findByUserAndFollower(diabetesUser, followerUser);
		if (null != repoFollower) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
		
		repoFollower = new DiabetesFollower(diabetesUser, followerUser);
		repoFollower.setApproved(true);
		return followerRepository.save(repoFollower);
	}
	
	@RequestMapping(value = WATCHLIST_PATH + "/add/{username}", method = RequestMethod.POST)
	public @ResponseBody DiabetesFollower addToWatchlist(
			@PathVariable("username") String username,
			Principal principal,
			HttpServletResponse response) throws IOException {
		
		DiabetesUser watchlistUser = diabetesUserRepository.findByUsername(username.toLowerCase());
		DiabetesUser diabetesUser = diabetesUserRepository.findByUsername(principal.getName());
		
		if (null == watchlistUser || null == diabetesUser) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		
		if (diabetesUser.getId() == watchlistUser.getId() || !watchlistUser.isTeen()) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
		
		DiabetesFollower repoFollower = followerRepository.findByUserAndFollower(watchlistUser, diabetesUser);
		if (null != repoFollower) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
		
		repoFollower = new DiabetesFollower(watchlistUser, diabetesUser);
		repoFollower.setApproved(false);
		return followerRepository.save(repoFollower);
	}
	
	@RequestMapping(value = FOLLOWER_PATH, method = RequestMethod.POST)
	public @ResponseBody DiabetesFollower setFollower(@RequestBody DiabetesFollower follower,
			Principal principal, HttpServletResponse response) throws IOException {
		
		DiabetesUser principalUser = diabetesUserRepository.findByUsername(principal.getName());
		
		if (null == principalUser) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		
		DiabetesFollower repoFollower = followerRepository.findOne(follower.getId());
		
		if (principalUser.getId() != repoFollower.getUid() || null == repoFollower) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		
		repoFollower.setFields(follower);
		return followerRepository.save(repoFollower);
	}
	
	@RequestMapping(value = FOLLOWER_REMOVE_PATH, method = RequestMethod.POST)
	public void removeFollower(@RequestBody DiabetesFollower follower,
			Principal principal, HttpServletResponse response) throws IOException {
		
		DiabetesUser principalUser = diabetesUserRepository.findByUsername(principal.getName());
		
		if (null == principalUser) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		DiabetesFollower repoFollower = followerRepository.findOne(follower.getId());
		
		if (null == repoFollower) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		if (repoFollower.getUid() != principalUser.getId() && repoFollower.getFollowerUid() != principalUser.getId()) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		followerRepository.delete(repoFollower);
	}
	
}
