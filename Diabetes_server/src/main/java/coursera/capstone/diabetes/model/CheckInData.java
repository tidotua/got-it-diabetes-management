package coursera.capstone.diabetes.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class CheckInData {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private long id;
	
	private long uid;
	private long dateTime;
	private short bloodSugarLevel;
	private boolean administerInsulin;
	private short mood;
	private String mealText;
	
	@JsonProperty
	public long getId() {
		return id;
	}
	
	@JsonIgnore
	public void setId(long id) {
		this.id = id;
	}
	
	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public long getDateTime() {
		return dateTime;
	}

	public void setDateTime(long datetime) {
		this.dateTime = datetime;
	}

	public short getBloodSugarLevel() {
		return bloodSugarLevel;
	}

	public void setBloodSugarLevel(short bloodSugarLevel) {
		this.bloodSugarLevel = bloodSugarLevel;
	}

	public boolean isAdministerInsulin() {
		return administerInsulin;
	}

	public void setAdministerInsulin(boolean administerInsulin) {
		this.administerInsulin = administerInsulin;
	}

	public short getMood() {
		return mood;
	}

	public void setMood(short mood) {
		this.mood = mood;
	}

	public String getMealText() {
		return mealText;
	}

	public void setMealText(String mealText) {
		this.mealText = mealText;
	}
}
