package coursera.capstone.diabetes.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
public class DiabetesUser implements UserDetails {

	private static final long serialVersionUID = 6619401904863514143L;

	public static DiabetesUser create(String username, String password,
			Collection<GrantedAuthority> authorities) {
		return new DiabetesUser(username, password, authorities);
	}

	public static DiabetesUser create(String username, String password,
			String...authorities) {
		return new DiabetesUser(username, password, authorities);
	}
	
	public final static int TEEN_TYPE = 0;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private long id;
	
	private String username;
	
	@JsonIgnore
	private String password;
	
	@JsonIgnore
	@ElementCollection(fetch=FetchType.EAGER)
	private Set<String> authorities = new HashSet<String>();
	
	private String firstName;
	private String lastName;
	private long birthDay;
	private String medicalRecord;
	private long type;
	private long notificationPeriodMs;
	private boolean notificationIgnoreNight;
	
	public DiabetesUser() {
		
	}	
	
	private DiabetesUser(String username, String password,
			Collection<GrantedAuthority> authorities) {
		this.username = username;
		this.password = password;
		this.authorities = AuthorityUtils.authorityListToSet(authorities);
	}
	
	private DiabetesUser(String username, String password,
			String...authorities) {
		this.username = username;
		this.password = password;
		setAuthorities(authorities);
	}
	
	public void update(String username, String password,
			Collection<GrantedAuthority> authorities) {
		this.username = username;
		this.password = password;
		this.authorities.clear();
		this.authorities = AuthorityUtils.authorityListToSet(authorities);
	}
	
	public void setFields(DiabetesUser source) {
		this.firstName = source.firstName;
		this.lastName = source.lastName;
		this.birthDay = source.birthDay;
		this.medicalRecord = source.medicalRecord;
		this.type = source.type;
		this.notificationPeriodMs = source.notificationPeriodMs;
		this.notificationIgnoreNight = source.notificationIgnoreNight;
	}
	
	@JsonProperty
	public long getId() {
		return id;
	}
	
	@JsonIgnore
	public void setId(long id) {
		this.id = id;
	}
	
	public Collection<GrantedAuthority> getAuthorities() {
		//return AuthorityUtils.createAuthorityList("USER");
		return AuthorityUtils.createAuthorityList(authorities.toArray(new String[authorities.size()]));
	}
	
	public void setAuthorities(String...authorities) {
		this.authorities = AuthorityUtils.authorityListToSet(AuthorityUtils.createAuthorityList(authorities));
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	@JsonIgnore
	public String getPassword() {
		return password;
	}
	
	@JsonProperty
	public void setPassword(String newPassword) {
		this.password = newPassword;		
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@JsonIgnore
	public String getFullName() {
		return (getFirstName() + " " + getLastName()).trim();
	}

	public long getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(long birthDay) {
		this.birthDay = birthDay;
	}

	public String getMedicalRecord() {
		return medicalRecord;
	}

	public void setMedicalRecord(String medicalRecord) {
		this.medicalRecord = medicalRecord;
	}

	public long getType() {
		return type;
	}
	
	public void setType(long type) {
		this.type = type;
	}
	
	@JsonIgnore
	public boolean isTeen() {
		return TEEN_TYPE == type;
	}
	
	public long getNotificationPeriodMs() {
		return notificationPeriodMs;
	}

	public void setNotificationPeriodMs(long notificationPeriodMs) {
		this.notificationPeriodMs = notificationPeriodMs;
	}

	public boolean isNotificationIgnoreNight() {
		return notificationIgnoreNight;
	}

	public void setNotificationIgnoreNight(boolean notificationIgnoreNight) {
		this.notificationIgnoreNight = notificationIgnoreNight;
	}
	
	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	
	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	
	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	
	@JsonIgnore
	@Override
	public boolean isEnabled() {
		return true;
	}
}
