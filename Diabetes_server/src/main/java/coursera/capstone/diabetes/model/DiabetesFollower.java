package coursera.capstone.diabetes.model;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class DiabetesFollower {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "uid")
	@JsonIgnore
	private DiabetesUser user;

	@ManyToOne
	@JoinColumn(name = "followerUid")
	@JsonIgnore
	private DiabetesUser follower;
	
	private boolean approved = false;
	private boolean showMood = false;
	private boolean showMeal = false;
	private boolean showAdministerInsuline = false;
	
	@Transient
	@JsonIgnore
	private String fullName;
	
	public DiabetesFollower() {
		super();
	}
	
	public DiabetesFollower(DiabetesUser user, DiabetesUser follower) {
		super();
		this.user = user;
		this.follower = follower;
	}
	
	public void setFields(DiabetesFollower source) {
		this.approved = source.approved;
		this.showMood = source.showMood;
		this.showMeal = source.showMeal;
		this.showAdministerInsuline = source.showAdministerInsuline;
	}
	
	public Collection<CheckInData> filterFeedbackData(Collection<CheckInData> feedback) {
		for (CheckInData checkInData: feedback) {
			if (!showMood) {
				checkInData.setMood((short)2);
			}
			if (!showMeal) {
				checkInData.setMealText("");
			}
			if (!showAdministerInsuline) {
				checkInData.setAdministerInsulin(false);
			}
		}
		
		return feedback;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@JsonProperty
	public long getUid() {
		return user.getId();
	}
	
	@JsonIgnore
	public void setUid(long uid) {
		// to Json ignore only
	}
	
	@JsonProperty
	public long getFollowerUid() {
		return follower.getId();
	}
	
	@JsonProperty
	public void setFollowerUid(long followerUid) {
		// to Json ignore only
	}
	
	@JsonIgnore
	public DiabetesUser getUser() {
		return user;
	}
	
	@JsonIgnore
	public DiabetesUser getFollower() {
		return follower;
	}
	
	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean isApproved) {
		this.approved = isApproved;
	}

	public boolean isShowMood() {
		return showMood;
	}

	public void setShowMood(boolean showMood) {
		this.showMood = showMood;
	}

	public boolean isShowMeal() {
		return showMeal;
	}

	public void setShowMeal(boolean showMeal) {
		this.showMeal = showMeal;
	}

	public boolean isShowAdministerInsuline() {
		return showAdministerInsuline;
	}

	public void setShowAdministerInsuline(boolean showAdministerInsuline) {
		this.showAdministerInsuline = showAdministerInsuline;
	}
	
	@JsonProperty
	public String getFullName() {
		return fullName;
	}
	
	@JsonIgnore
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
}
