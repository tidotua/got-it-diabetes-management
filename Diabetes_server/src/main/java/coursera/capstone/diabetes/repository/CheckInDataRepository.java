package coursera.capstone.diabetes.repository;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import coursera.capstone.diabetes.model.CheckInData;

@Repository
public interface CheckInDataRepository extends CrudRepository<CheckInData, Long> {
	public List<CheckInData> findByUidOrderByDateTimeDesc(long uid, Pageable pageable);
}
