package coursera.capstone.diabetes.repository;

import java.util.Collection;

import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

import coursera.capstone.diabetes.model.DiabetesFollower;
import coursera.capstone.diabetes.model.DiabetesUser;


@Repository
public interface DiabetesFollowerRepository extends CrudRepository<DiabetesFollower, Long> {
	public Collection<DiabetesFollower> findByUserOrderByFollowerFirstNameAscFollowerLastNameAsc(DiabetesUser user);
	public Collection<DiabetesFollower> findByFollowerAndUserTypeOrderByUserFirstNameAscUserLastNameAsc(DiabetesUser follower, long userType);

	public DiabetesFollower findByUserAndFollower(DiabetesUser user, DiabetesUser follower);
}
