package coursera.capstone.diabetes.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

import coursera.capstone.diabetes.model.DiabetesUser;

@Repository
public interface DiabetesUserRepository extends CrudRepository<DiabetesUser, Long> {
	public DiabetesUser findByUsername(String username);
}
