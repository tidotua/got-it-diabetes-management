package coursera.capstone.diabetes.auth;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.util.Assert;

import coursera.capstone.diabetes.model.DiabetesUser;
import coursera.capstone.diabetes.repository.DiabetesUserRepository;

public class PersistenceUserDetailsManager implements UserDetailsManager {
    
	protected final Log logger = LogFactory.getLog(getClass());
	
    private AuthenticationManager authenticationManager;
    private DiabetesUserRepository userRepository;
    
    @Override
    @SuppressWarnings("unchecked")
	public void createUser(UserDetails userDetails) {
        Assert.isTrue(!userExists(userDetails.getUsername()));

        DiabetesUser user = DiabetesUser.create(userDetails.getUsername().toLowerCase(), userDetails.getPassword(), 
        		(Collection<GrantedAuthority>)userDetails.getAuthorities());
        userRepository.save(user);
    }
    
    @Override
    public void deleteUser(String username) {
    	DiabetesUser user = userRepository.findByUsername(username.toLowerCase());
    	if (null != user) {
    		userRepository.delete(user);
    	}
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public void updateUser(UserDetails userDetails) {
    	DiabetesUser user = userRepository.findByUsername(userDetails.getUsername().toLowerCase());
    	if (null != user) {
    		user.update(userDetails.getUsername().toLowerCase(), userDetails.getPassword(), 
            		(Collection<GrantedAuthority>)userDetails.getAuthorities());    		
    		userRepository.save(user);
    	}
    }
    
    @Override
    public boolean userExists(String username) {
    	return userRepository.findByUsername(username.toLowerCase()) != null;
    }
    
    @Override
    public void changePassword(String oldPassword, String newPassword) {
        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();

        if (currentUser == null) {
            throw new AccessDeniedException("Can't change password as no Authentication object found in context " +
                    "for current user.");
        }

        String username = currentUser.getName();

        if (authenticationManager != null) {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, oldPassword));
        }
        
        DiabetesUser user = userRepository.findByUsername(username);
        if (user == null) {
            throw new IllegalStateException("Current user doesn't exist in database.");
        }

        user.setPassword(newPassword);
        userRepository.save(user);
    }
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	DiabetesUser user = userRepository.findByUsername(username.toLowerCase());

        if (user == null) {
        	logger.debug("No user found '"+ username + "'");
        	throw new UsernameNotFoundException(username);
        }
        
        return new User(user.getUsername().toLowerCase(), user.getPassword(), user.isEnabled(), user.isAccountNonExpired(),
                user.isCredentialsNonExpired(), user.isAccountNonLocked(), user.getAuthorities());
    }

    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }
    
    public void setUserRepository(DiabetesUserRepository userRepository) {
		this.userRepository = userRepository;
	}
}
